import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String formatCurrency(double money, String currencyLabel) {
  final currencyFormatter =
      NumberFormat("#,##0"); // Using en_US locale for the comma separator

  String formattedMoney = currencyFormatter.format(money);

  return '$formattedMoney $currencyLabel'.replaceAll(',', '.');
}

String formatDate({required date, required String formatDate}) {
  return DateFormat(formatDate).format(date);
}

String calculateDurationString(DateTime start, DateTime end) {
  Duration duration = end.difference(start);

  if (duration.inHours < 1) {
    // If duration is less than one hour, return only minutes
    int minutes = duration.inMinutes;
    return '$minutes minutes';
  } else {
    // Otherwise, return hours:minutes:seconds
    int hours = duration.inHours;
    int minutes = (duration.inMinutes % 60);
    if (minutes == 0) {
      // If minutes are zero, only show hours
      return '$hours hours';
    } else {
      return '$hours hours ${_twoDigits(minutes)} minutes';
    }
  }
}

String _twoDigits(int n) {
  if (n >= 10) return '$n';
  return '0$n';
}

String formatTimeOfDay(TimeOfDay timeOfDay) {
  return '${timeOfDay.hour}:${timeOfDay.minute.toString().padLeft(2, '0')}';
}

// Function to check if a TimeOfDay is valid
bool isValidTimeOfDay(TimeOfDay timeOfDay) {
  return timeOfDay.hour >= 0 &&
      timeOfDay.hour < 24 &&
      timeOfDay.minute >= 0 &&
      timeOfDay.minute < 60;
}

// Function to check if the time range is valid (end time after start time)
bool isValidTimeRange(TimeOfDay startTime, TimeOfDay endTime) {
  if (isValidTimeOfDay(startTime) && isValidTimeOfDay(endTime)) {
    final startMinutes = startTime.hour * 60 + startTime.minute;
    final endMinutes = endTime.hour * 60 + endTime.minute;
    return endMinutes > startMinutes;
  }
  return false;
}

// Function to get a list of formatted time strings in the range
List<String> getTimeRangeStrings(TimeOfDay start, TimeOfDay end) {
  List<String> timeList = [];

  while (start.hour < end.hour ||
      (start.hour == end.hour && start.minute <= end.minute)) {
    timeList.add(formatTimeOfDay(start));
    start = TimeOfDay(hour: start.hour + 1, minute: start.minute);
  }

  return timeList;
}

TimeOfDay? getStartTime(List<String> timeList) {
  if (timeList.isNotEmpty) {
    final startTimeString = timeList.first;
    return parseTimeString(startTimeString);
  }
  return null;
}

// Function to get the end TimeOfDay from a list of time strings
TimeOfDay? getEndTime(List<String> timeList) {
  if (timeList.isNotEmpty) {
    final endTimeString = timeList.last;
    return parseTimeString(endTimeString);
  }
  return null;
}

// Function to parse a time string and return a TimeOfDay

DateTime? setDateTimeTime(DateTime originalDateTime, String timeString) {
  TimeOfDay timeOfDay = parseTimeString(timeString) ?? TimeOfDay.now();
  return DateTime(
    originalDateTime.year,
    originalDateTime.month,
    originalDateTime.day,
    timeOfDay.hour,
    timeOfDay.minute,
  );
}

// Function to parse a time string and return a TimeOfDay
TimeOfDay? parseTimeString(String timeString) {
  final List<String> parts = timeString.split(':');
  if (parts.length == 2) {
    final int hour = int.parse(parts[0]);
    final int minute = int.parse(parts[1]);
    return TimeOfDay(hour: hour, minute: minute);
  }
  return null;
}
