import 'package:app/gen/assets.gen.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class SearchWidget extends StatefulWidget {
  final TextEditingController textEditingController;
  final String? hintText;
  final VoidCallback? onFilterTap;
  final VoidCallback? onAddTap;
  final bool showFilterIcon;
  final Function(String value) onChanged;

  const SearchWidget(
      {super.key,
      required this.textEditingController,
      required this.onChanged,
      this.hintText,
      this.onAddTap,
      this.onFilterTap,
      this.showFilterIcon = true});

  @override
  State<SearchWidget> createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30.w),
      height: 50.h,
      child: Row(
        children: [
          widget.showFilterIcon
              ? GestureDetector(
                  onTap: () {
                    widget.onFilterTap?.call();
                  },
                  child: Assets.svg.icFilter
                      .svg(width: 20.w)
                      .marginOnly(right: 20.w))
              : const SizedBox.shrink(),
          Expanded(
              child: TextFormField(
            onChanged: (value) {
              widget.onChanged.call(value);
            },
            onSaved: (value) {
              widget.onChanged.call(widget.textEditingController.value.text);
            },
            controller: widget.textEditingController,
            style: TextStyle(
                fontFamily: ThemeProvider.fontRegular,
                fontSize: ThemeProvider.fontSize14,
                color: ThemeProvider.colorBlack),
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
                hintText: widget.hintText,
                hintStyle: TextStyle(
                    fontFamily: ThemeProvider.fontRegular,
                    fontSize: ThemeProvider.fontSize14,
                    color: ThemeProvider.colorHintText),
                isDense: true,
                contentPadding: EdgeInsets.symmetric(horizontal: 16.w),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(16)),
                suffixIcon: Padding(
                    padding: EdgeInsets.all(14.w),
                    child: Assets.svg.icSearch.svg(
                        width: 12,
                        height: 12,
                        colorFilter: const ColorFilter.mode(
                            ThemeProvider.textColorButtonDisable,
                            BlendMode.srcIn)))),
          )),
          GestureDetector(
            onTap: () {
              widget.onAddTap?.call();
            },
            child: Assets.svg.icAdd.svg().marginOnly(left: 20.w),
          )
        ],
      ),
    );
  }
}
