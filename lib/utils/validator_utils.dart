import 'package:get/get.dart';

class ValidatorUtils {
  ValidatorUtils._();

  static String? validatorPhone(String? text) {
    if (text?.isEmpty == true) {
      return 'empty_phone'.tr;
    }
    if (!GetUtils.isPhoneNumber(text ?? '')) {
      return 'invalid_phone'.tr;
    }
    return null;
  }

  static String? validatorPass(String? text) {
    if (text?.isEmpty == true) {
      return 'empty_pass'.tr;
    }
    if (!RegExp(r'^.{6,}$').hasMatch(text ?? '')) {
      return 'invalid_pass'.tr;
    }
    return null;
  }

  static String? validatorRePass(String? pass, String? repass) {
    if (repass?.isEmpty == true) {
      return 'empty_pass'.tr;
    }
    if (!RegExp(r'^.{6,}$').hasMatch(repass ?? '')) {
      return 'invalid_pass'.tr;
    }
    if (repass != pass) {
      return 'pass_not_match'.tr;
    }
    return null;
  }

  static String? validatorEmail(String? text, {bool isRequired = false}) {
    if (text?.isEmpty == true) {
      return isRequired ? 'empty_email'.tr : null;
    }
    if (!GetUtils.isEmail(text ?? '')) {
      return 'invalid_email'.tr;
    }
    return null;
  }

  static String? validatorName(String? text) {
    if (text?.isEmpty == true) {
      return 'empty_name'.tr;
    }
    if ((text?.length ?? 0) < 6) {
      return 'invalid_name'.tr;
    }
    return null;
  }
}
