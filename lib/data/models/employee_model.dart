import 'package:json_annotation/json_annotation.dart';
part 'employee_model.g.dart';

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: true,
)
class EmployeeModel {
  int? id;
  String? name;
  String? phone;
  String? email;

  EmployeeModel({this.name, this.email, this.phone, this.id});
  factory EmployeeModel.fromJson(Map<String, dynamic> json) =>
      _$EmployeeModelFromJson(json);

  Map<String, dynamic> toJson() => _$EmployeeModelToJson(this);
}
