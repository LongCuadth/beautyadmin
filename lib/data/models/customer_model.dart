import 'package:json_annotation/json_annotation.dart';
part 'customer_model.g.dart';

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: true,
)
class CustomerModel {
  int? id;
  String? name;
  String? phone;
  String? email;

  CustomerModel({this.name, this.email, this.phone, this.id});
  factory CustomerModel.fromJson(Map<String, dynamic> json) =>
      _$CustomerModelFromJson(json);  

  Map<String, dynamic> toJson() => _$CustomerModelToJson(this);
}
