import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'services_model.g.dart';

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: true,
)
class ServicesModel {
  int? id;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'desc')
  String? description;
  String? price;
  @JsonKey(name: 'time')
  int? duration; // in minutes
  @JsonKey(name: 'service_id')
  int? idCategory;
  @JsonKey(name: 'users')
  List<int>? employee;

  ServicesModel(
      {this.name,
      this.id,
      this.price,
      this.duration,
      this.idCategory,
      this.employee,
      this.description});

  factory ServicesModel.fromJson(Map<String, dynamic> json) =>
      _$ServicesModelFromJson(json);

  Map<String, dynamic> toJson() => _$ServicesModelToJson(this);

  Map<String, dynamic> toCreateModelJson() => <String, dynamic>{
        'name': name,
        'desc': description,
        'price': price,
        'time': duration,
        'category_id': 1,
        'service_id': idCategory,
        'users': json.encode(employee)
      };
}
