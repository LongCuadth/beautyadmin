import 'package:json_annotation/json_annotation.dart';

part 'shop_model.g.dart';

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: true,
)
class ShopModel {
  int? id;
  String? name;
  String? code;
  String? address;
  String? phone;
  String? image;
  List<String>? times;

  ShopModel({
    this.id,
    this.name,
    this.code,
    this.address,
    this.phone,
    this.times,
    this.image,
  });

  factory ShopModel.fromJson(Map<String, dynamic> json) =>
      _$ShopModelFromJson(json);

  Map<String, dynamic> toJson() => _$ShopModelToJson(this);
}
