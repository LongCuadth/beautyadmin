import 'package:json_annotation/json_annotation.dart';

part 'token_model.g.dart';

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: true,
)
class TokenModel {
  @JsonKey(name: 'access_token')
 String? accessToken;
  @JsonKey(name: 'token_type')
  String? tokenType;
  @JsonKey(name: 'expires_in')
  int? expiresIn;

 TokenModel({
   this.accessToken,
   this.tokenType,
   this.expiresIn
  });

  factory TokenModel.fromJson(Map<String, dynamic> json) =>
      _$TokenModelFromJson(json);

  Map<String, dynamic> toJson() => _$TokenModelToJson(this);
}
