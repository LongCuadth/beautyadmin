import 'package:app/data/models/customer_model.dart';
import 'package:app/data/models/employee_model.dart';
import 'package:app/data/models/services_model.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_annotation/json_annotation.dart';

part 'booking_model.g.dart';

@JsonSerializable(
  createToJson: true,
)
class BookingModel {
  int? id;
  @JsonKey(name: 'user')
  CustomerModel? customer;
  @JsonKey(name: 'products')
  List<ServicesModel>? listProduct;
  EmployeeModel? employee;
  DateTime? date;
  @JsonKey(name: 'time_start')
  String? startTime;
  @JsonKey(name: 'time_end')
  String? endTime;
  String? price;
  @JsonKey(name: 'total_time')
  int? duration;
  int? status; // 2: approve, 3: checkin, 4: payment
  String? name;

  BookingModel(
      {this.id,
      this.customer,
      this.name,
      this.status,
      this.listProduct,
      this.employee,
      this.startTime,
      this.price,
      this.date,
      this.duration,
      this.endTime});
  factory BookingModel.fromJson(Map<String, dynamic> json) =>
      _$BookingModelFromJson(json);

  Map<String, dynamic> toJson() => _$BookingModelToJson(this);
}

Color getCourseStatusColor(int status) {
  if (status == 2) {
    return ThemeProvider.colorPrimary;
  } else if (status == 3) {
    return ThemeProvider.colorErrorText;
  } else if (status == 4) {
    return Colors.green.withOpacity(0.8);
  }
  return Colors.brown.withOpacity(0.8);
}

String getCourseStatusName(int status) {
  if (status == 2) {
    return 'confirm_booking'.tr;
  } else if (status == 3) {
    return 'checkin'.tr;
  } else if (status == 4) {
    return 'payment'.tr;
  }
  return 'waiting_approve'.tr;
}
