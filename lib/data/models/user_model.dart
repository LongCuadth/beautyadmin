import 'package:app/data/models/shop_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable(
  genericArgumentFactories: true,
  createToJson: true,
)
class UserModel {
  int? id;
  String? email;
  String? phone;
  String? name;
  String? image;
  ShopModel? shop;

  UserModel({
    this.id,
    this.email,
    this.phone,
    this.name,
    this.image,
    this.shop,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
