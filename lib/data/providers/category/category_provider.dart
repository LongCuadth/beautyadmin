import 'package:app/common/network/config.dart';
import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/models/base/base_list_model.dart';
import 'package:app/data/models/base/base_model.dart';
import 'package:app/data/models/category_model.dart';

import 'package:app/data/providers/base_provider.dart';

abstract class ICategoryProvider {
  Future<BaseListModel<CategoryModel>> getListCategory(
      Map<String, dynamic> payload);
  Future<BaseModel> createCategory(Map<String, dynamic> payload);
  Future<BaseModel> deleteCategory(int idCategory);
  Future<BaseModel<CategoryModel>> updateCategory(
      int idCategory, Map<String, dynamic> payload);
}

class CategoryProvider extends BaseProvider implements ICategoryProvider {
  @override
  Future<BaseModel> createCategory(Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendPostRequest(
        '/api/${shop?.id}/service', payload, null);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(response, (jsonModel) {
      return [];
    });
  }

  @override
  Future<BaseModel> deleteCategory(int idCategory) async {
    var response = await httpClient.sendDeleteRequest(
        ApiPath.deleteCategory + idCategory.toString(), null);
    return BaseModel.fromJson(response, (jsonModel) {});
  }

  @override
  Future<BaseListModel<CategoryModel>> getListCategory(
      Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendGetRequest(
        '/api/${shop?.id}/service', payload, null);
    if (response == null) return BaseListModel(success: false);
    return BaseListModel.fromJson(response, (jsonModel) {
      if (jsonModel.isNotEmpty) {
        return CategoryModel.fromJson(jsonModel);
      }
      return CategoryModel();
    });
  }

  @override
  Future<BaseModel<CategoryModel>> updateCategory(
      int idCategory, Map<String, dynamic> payload) async {
    var response = await httpClient.sendPutRequest(
        ApiPath.updateCategory + idCategory.toString(), payload);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(
        response, (jsonModel) => CategoryModel.fromJson(jsonModel));
  }
}
