import 'package:app/common/network/config.dart';
import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/models/base/base_list_model.dart';
import 'package:app/data/models/base/base_model.dart';
import 'package:app/data/models/employee_model.dart';

import 'package:app/data/providers/base_provider.dart';

abstract class IEmployeeProvider {
  Future<BaseListModel<EmployeeModel>> getListEmployee(
      Map<String, dynamic> payload);
  Future<BaseModel> createEmployee(Map<String, dynamic> payload);
  Future<BaseModel> deleteEmployee(int isEmployee);
  Future<BaseModel<EmployeeModel>> updateEmployee(
      int isEmployee, Map<String, dynamic> payload);
}

class EmployeeProvider extends BaseProvider implements IEmployeeProvider {
  @override
  Future<BaseModel> createEmployee(Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendPostRequest(
        '/api/${shop?.id}/user', payload, payload);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(response, (jsonModel) {
      return [];
    });
  }

  @override
  Future<BaseListModel<EmployeeModel>> getListEmployee(
      Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response =
        await httpClient.sendGetRequest('/api/${shop?.id}/user', payload, null);
    if (response == null) return BaseListModel(success: false);
    return BaseListModel.fromJson(response, (jsonModel) {
      if (jsonModel.isNotEmpty) {
        return EmployeeModel.fromJson(jsonModel);
      }
      return EmployeeModel();
    });
  }

  @override
  Future<BaseModel> deleteEmployee(int isEmployee) async {
    var response = await httpClient.sendDeleteRequest(
        ApiPath.deleteEmployee + isEmployee.toString(), null);
    return BaseModel.fromJson(response, (jsonModel) {});
  }

  @override
  Future<BaseModel<EmployeeModel>> updateEmployee(
      int isEmployee, Map<String, dynamic> payload) async {
    var response = await httpClient.sendPutRequest(
        ApiPath.updateEmployee + isEmployee.toString(), payload);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(
        response, (jsonModel) => EmployeeModel.fromJson(jsonModel));
  }
}
