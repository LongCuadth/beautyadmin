import 'package:app/common/network/config.dart';
import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/models/base/base_list_model.dart';
import 'package:app/data/models/base/base_model.dart';
import 'package:app/data/models/services_model.dart';
import 'package:app/data/providers/base_provider.dart';

abstract class IServicesProvider {
  Future<BaseListModel<ServicesModel>> getListService(
      Map<String, dynamic> payload);
  Future<BaseModel> createService(Map<String, dynamic> payload);
  Future<BaseModel> deleteService(int idService);
  Future<BaseModel<ServicesModel>> updateServices(
      int idService, Map<String, dynamic> payload);
}

class ServicesProvider extends BaseProvider implements IServicesProvider {
  @override
  Future<BaseModel> createService(Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendPostRequest(
        '/api/${shop?.id}/product', payload, null);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(response, (jsonModel) {
      return [];
    });
  }

  @override
  Future<BaseListModel<ServicesModel>> getListService(
      Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendGetRequest(
        '/api/${shop?.id}/product', payload, null);
    if (response == null) return BaseListModel(success: false);
    return BaseListModel.fromJson(response, (jsonModel) {
      if (jsonModel.isNotEmpty) {
        return ServicesModel.fromJson(jsonModel);
      }
      return ServicesModel();
    });
  }

  @override
  Future<BaseModel> deleteService(int idService) async {
    var response = await httpClient.sendDeleteRequest(
        ApiPath.deleteService + idService.toString(), null);
    return BaseModel.fromJson(response, (jsonModel) {});
  }

  @override
  Future<BaseModel<ServicesModel>> updateServices(
      int idService, Map<String, dynamic> payload) async {
    var response = await httpClient.sendPutRequest(
        ApiPath.updateService + idService.toString(), payload);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(
        response, (jsonModel) => ServicesModel.fromJson(jsonModel));
  }
}
