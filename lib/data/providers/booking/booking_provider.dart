import 'package:app/common/network/config.dart';
import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/models/base/base_list_model.dart';
import 'package:app/data/models/base/base_model.dart';
import 'package:app/data/models/booking_model.dart';

import 'package:app/data/providers/base_provider.dart';

abstract class IBookingProvider {
  Future<BaseListModel<BookingModel>> getListBooking(
      Map<String, dynamic> payload);
  Future<BaseModel> createBooking(Map<String, dynamic> payload);
  Future<BaseModel> deleteBooking(int idBooking);
  Future<BaseModel<BookingModel>> updateBooking(
      int idBooking, Map<String, dynamic> payload);
}

class BookingProvider extends BaseProvider implements IBookingProvider {
  @override
  Future<BaseModel> createBooking(Map<String, dynamic> payload) async {
    var shop = SharedPreference.shared.getShop();
    var response = await httpClient.sendPostRequest(
        '/api/${shop?.id}/order', payload, null);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(response, (jsonModel) {
      return [];
    });
  }

  @override
  Future<BaseModel> deleteBooking(int idBooking) async {
    var response = await httpClient.sendDeleteRequest(
        ApiPath.deleteBooking + idBooking.toString(), null);
    return BaseModel.fromJson(response, (jsonModel) {});
  }

  @override
  Future<BaseListModel<BookingModel>> getListBooking(
      Map<String, dynamic> payload) async {
    var response =
        await httpClient.sendGetRequest(ApiPath.getListBooking, payload, null);
    if (response == null) return BaseListModel(success: false);
    return BaseListModel.fromJson(response, (jsonModel) {
      if (jsonModel.isNotEmpty) {
        return BookingModel.fromJson(jsonModel);
      }
      return BookingModel();
    });
  }

  @override
  Future<BaseModel<BookingModel>> updateBooking(
      int idBooking, Map<String, dynamic> payload) async {
    var response = await httpClient.sendPostRequest(
        ApiPath.updateBooking + idBooking.toString(), payload, null);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(
        response, (jsonModel) => BookingModel.fromJson(jsonModel));
  }
}
