import 'package:app/common/network/config.dart';
import 'package:app/data/models/base/base_model.dart';
import 'package:app/data/models/shop_model.dart';
import 'package:app/data/models/token_model.dart';
import 'package:app/data/models/user_model.dart';
import 'package:app/data/providers/base_provider.dart';

abstract class IAuthProvider {
  Future<BaseModel<TokenModel?>> login(Map<String, dynamic> payload);

  Future<BaseModel> checkPhoneExits(Map<String, dynamic> payload);

  Future<BaseModel<UserModel?>> getUserInfo();

  Future<BaseModel> logOut();

  Future<BaseModel<ShopModel?>> updateShop(
      int idShop, Map<String, dynamic> payload);
}

class AuthProvider extends BaseProvider implements IAuthProvider {
  @override
  Future<BaseModel<TokenModel?>> login(Map<String, dynamic> payload) async {
    var response =
        await httpClient.sendPostRequest(ApiPath.apiLogin, payload, null);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(response, (jsonModel) {
      return TokenModel.fromJson(jsonModel);
    });
  }

  @override
  Future<BaseModel> checkPhoneExits(Map<String, dynamic> payload) async {
    var response =
        await httpClient.sendPostRequest(ApiPath.checkphone, payload, null);
    if (response == null) return BaseModel(success: false);
    return BaseModel.fromJson(response, (jsonModel) => null);
  }

  @override
  Future<BaseModel<UserModel?>> getUserInfo() async {
    var response =
        await httpClient.sendGetRequest(ApiPath.apiGetUserInfo, null, null);
    if (response == null) {
      return BaseModel(success: false);
    }
    return BaseModel.fromJson(response, (json) => UserModel.fromJson(json));
  }

  @override
  Future<BaseModel> logOut() async {
    var response =
        await httpClient.sendPostRequest(ApiPath.apiLogout, null, null);
    if (response == null) {
      return BaseModel(success: false);
    }
    return BaseModel(success: true);
  }

  @override
  Future<BaseModel<ShopModel?>> updateShop(
      int idShop, Map<String, dynamic> payload) async {
    var response = await httpClient.sendPutRequest(
        ApiPath.apiUpdateShop + idShop.toString(), payload);
    if (response == null) {
      return BaseModel(success: false);
    }
    return BaseModel.fromJson(
        response, (jsonModel) => ShopModel.fromJson(jsonModel));
  }
}
