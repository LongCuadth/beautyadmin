// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const SPLASH = _Paths.SPLASH;
  static const MAIN = _Paths.MAIN;
  static const HOME = _Paths.HOME;
  static const CATEGORY = _Paths.CATEGORY;
  static const NOTIFICATION = _Paths.NOTIFICATION;
  static const ACCOUNT = _Paths.ACCOUNT;
  static const LOGIN = _Paths.LOGIN;
  static const REGISTER = _Paths.REGISTER;
  static const FORGOT_PASSWORD = _Paths.FORGOT_PASSWORD;
  static const CUSTOMER_MANAGER = _Paths.CUSTOMER_MANAGER;
  static const EMPLOYEE_MANAGER = _Paths.EMPLOYEE_MANAGER;
  static const SERVICES_MANAGER = _Paths.SERVICES_MANAGER;
  static const BOOKING_MANAGER = _Paths.BOOKING_MANAGER;
  static const CATEGORY_MANAGER = _Paths.CATEGORY_MANAGER;
  static const ADD_SERVICE = _Paths.ADD_SERVICE;
  static const EDIT_SERVICE = _Paths.EDIT_SERVICE;
  static const APPOINTMENT_MANAGER = _Paths.APPOINTMENT_MANAGER;
  static const SHOP_INFO = _Paths.SHOP_INFO;
  static const BANNER_MANAGER = _Paths.BANNER_MANAGER;
}

abstract class _Paths {
  _Paths._();
  static const SPLASH = '/splash';
  static const MAIN = '/main';
  static const HOME = '/home';
  static const CATEGORY = '/category';
  static const NOTIFICATION = '/notification';
  static const ACCOUNT = '/account';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const FORGOT_PASSWORD = '/forgot-password';
  static const CUSTOMER_MANAGER = '/customer-manager';
  static const EMPLOYEE_MANAGER = '/employee-manager';
  static const SERVICES_MANAGER = '/services-manager';
  static const BOOKING_MANAGER = '/booking-manager';
  static const CATEGORY_MANAGER = '/category-manager';
  static const ADD_SERVICE = '/add-service';
  static const EDIT_SERVICE = '/edit-service';
  static const APPOINTMENT_MANAGER = '/appointment-manager';
  static const SHOP_INFO = '/shop-info';
  static const BANNER_MANAGER = '/banner-manager';
}
