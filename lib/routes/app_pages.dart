import 'package:get/get.dart';

import '../../modules/splash/bindings/splash_binding.dart';
import '../../modules/splash/views/splash_view.dart';
import '../modules/add_service/bindings/add_service_binding.dart';
import '../modules/add_service/views/add_service_view.dart';
import '../modules/appointment_manager/bindings/appointment_manager_binding.dart';
import '../modules/appointment_manager/views/appointment_manager_view.dart';
import '../modules/banner_manager/bindings/banner_manager_binding.dart';
import '../modules/banner_manager/views/banner_manager_view.dart';
import '../modules/booking_manager/bindings/booking_manager_binding.dart';
import '../modules/booking_manager/views/booking_manager_view.dart';
import '../modules/category_manager/bindings/category_manager_binding.dart';
import '../modules/category_manager/views/category_manager_view.dart';
import '../modules/customer_manager/bindings/customer_manager_binding.dart';
import '../modules/customer_manager/views/customer_manager_view.dart';
import '../modules/employee_manager/bindings/employee_manager_binding.dart';
import '../modules/employee_manager/views/employee_manager_view.dart';
import '../modules/forgot_password/bindings/forgot_password_binding.dart';
import '../modules/forgot_password/views/forgot_password_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/main/bindings/main_binding.dart';
import '../modules/main/views/main_view.dart';
import '../modules/register/bindings/register_binding.dart';
import '../modules/register/views/register_view.dart';
import '../modules/services_manager/bindings/edit_services_binding.dart';
import '../modules/services_manager/bindings/services_manager_binding.dart';
import '../modules/services_manager/views/edit_services_view.dart';
import '../modules/services_manager/views/services_manager_view.dart';
import '../modules/shop_info/bindings/shop_info_binding.dart';
import '../modules/shop_info/views/shop_info_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.SPLASH,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER,
      page: () => const RegisterView(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: _Paths.FORGOT_PASSWORD,
      page: () => const ForgotPasswordView(),
      binding: ForgotPasswordBinding(),
    ),
    GetPage(
      name: _Paths.MAIN,
      page: () => const MainView(),
      binding: MainBinding(),
    ),
    GetPage(
      name: _Paths.CUSTOMER_MANAGER,
      page: () => const CustomerManagerView(),
      binding: CustomerManagerBinding(),
    ),
    GetPage(
      name: _Paths.EMPLOYEE_MANAGER,
      page: () => const EmployeeManagerView(),
      binding: EmployeeManagerBinding(),
    ),
    GetPage(
      name: _Paths.SERVICES_MANAGER,
      page: () => const ServicesManagerView(),
      binding: ServicesManagerBinding(),
    ),
    GetPage(
      name: _Paths.BOOKING_MANAGER,
      page: () => const BookingManagerView(),
      binding: BookingManagerBinding(),
    ),
    GetPage(
      name: _Paths.CATEGORY_MANAGER,
      page: () => const CategoryManagerView(),
      binding: CategoryManagerBinding(),
    ),
    GetPage(
      name: _Paths.ADD_SERVICE,
      page: () => const AddServiceView(),
      binding: AddServiceBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_SERVICE,
      page: () => const EditServicesView(),
      binding: EditServicesBinding(),
    ),
    GetPage(
      name: _Paths.APPOINTMENT_MANAGER,
      page: () => const AppointmentManagerView(),
      binding: AppointmentManagerBinding(),
    ),
    GetPage(
      name: _Paths.SHOP_INFO,
      page: () => const ShopInfoView(),
      binding: ShopInfoBinding(),
    ),
    GetPage(
      name: _Paths.BANNER_MANAGER,
      page: () => const BannerManagerView(),
      binding: BannerManagerBinding(),
    ),
  ];
}
