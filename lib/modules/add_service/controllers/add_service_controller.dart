import 'package:app/data/models/category_model.dart';
import 'package:app/data/models/employee_model.dart';
import 'package:app/data/models/services_model.dart';
import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/providers/service/service_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class AddServiceController extends GetxController {
  var nameController = TextEditingController();
  var detailController = TextEditingController();
  var priceController = TextEditingController();
  var minutesController = TextEditingController();
  var selectedCategory = Rx<CategoryModel?>(null);
  var listEmployee = <EmployeeModel>[].obs;
  var listEmployeeSelected = <EmployeeModel>[].obs;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _messageService = Get.find<MessageService>();
  var listCategory = <CategoryModel>[].obs;

  final IServicesProvider _serviceProvider = Get.find<IServicesProvider>();
  final ICategoryProvider _categoryProvider = Get.find<ICategoryProvider>();
  final IEmployeeProvider _employeeProvider = Get.find<IEmployeeProvider>();

  @override
  void onInit() {
    getData();
    super.onInit();
  }

  Future<void> getData() async {
    EasyLoading.show();
    // get Category
    var resCategory =
        await _categoryProvider.getListCategory({'all': 1, 'category_id': '1'});
    if (resCategory.success == true && resCategory.data != null) {
      listCategory.addAll(resCategory.data!.items ?? []);
    }
    // get Employee
    var resCustomer =
        await _employeeProvider.getListEmployee({'all': 1, 'type': '3'});
    listEmployee.addAll(resCustomer.data?.items ?? []);
    EasyLoading.dismiss();
  }

  void addServices() async {
    if (formKey.currentState!.validate()) {
      // if (selectedCategory.value != null) {
      var data = ServicesModel(
          name: nameController.text,
          duration: int.parse(minutesController.text),
          description: detailController.text,
          price: priceController.text,
          employee:
              listEmployeeSelected.map((element) => element.id ?? 0).toList(),
          idCategory: selectedCategory.value?.id ?? 0);
      var res = await _serviceProvider.createService(data.toCreateModelJson());
      if (res.success == true) {
        clearForm();
        _messageService.send(Message.success(content: 'create_success'.tr));
        // }
      } else {
        _messageService
            .send(Message.error(content: 'category_not_selected'.tr));
      }
    }
  }

  void clearForm() {
    nameController.clear();
    priceController.clear();
    minutesController.clear();
    detailController.clear();
    selectedCategory.value = null;
    listEmployeeSelected.clear();
  }
}
