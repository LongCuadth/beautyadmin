import 'package:app/data/providers/booking/booking_provider.dart';
import 'package:app/data/providers/customer/customer_provider.dart';
import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/providers/service/service_provider.dart';
import 'package:get/get.dart';

import '../controllers/booking_manager_controller.dart';

class BookingManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IBookingProvider>(() => BookingProvider());
    Get.lazyPut<IEmployeeProvider>(() => EmployeeProvider());
    Get.lazyPut<IServicesProvider>(() => ServicesProvider());
    Get.lazyPut<ICustomerProvider>(() => CustomerProvider());
    Get.lazyPut<BookingManagerController>(
      () => BookingManagerController(),
    );
  }
}
