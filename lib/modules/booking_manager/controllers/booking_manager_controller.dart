import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/models/booking_model.dart';
import 'package:app/data/providers/booking/booking_provider.dart';
import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/utils/format_util.dart';
import 'package:calendar_day_view/calendar_day_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class BookingManagerController extends GetxController {
  var currentDate = DateTime.now().obs;
  var listEmployeeCalendar = <EventCategory>[].obs;
  var listBooking = <CategorizedDayEvent<BookingModel>>[].obs;
  var startTimeOfDay = const TimeOfDay(hour: 8, minute: 0).obs;
  var endTimeOfDay = const TimeOfDay(hour: 20, minute: 0).obs;

  final IBookingProvider _bookingProvider = Get.find<IBookingProvider>();
  final IEmployeeProvider _employeeProvider = Get.find<IEmployeeProvider>();
  final MessageService _messageService = Get.find<MessageService>();

  @override
  void onInit() {
    EasyLoading.show();
    var listTime = SharedPreference.shared.getShop()?.times ?? [];
    startTimeOfDay.value =
        getStartTime(listTime) ?? const TimeOfDay(hour: 8, minute: 0);
    endTimeOfDay.value =
        getEndTime(listTime) ?? const TimeOfDay(hour: 20, minute: 0);
    getBookingData();
    getEmployeeData();
    EasyLoading.dismiss();
    super.onInit();
  }

  Future<void> getBookingData() async {
    // listCategoryCalendar.addAll(listEmployee.map((element) =>
    //     EventCategory(id: element.name ?? '', name: element.name ?? '')));
    listBooking.clear();
    var resData = await _bookingProvider.getListBooking({
      'page': 0,
      'size': 1000,
      'all': 1,
      'shop_id': SharedPreference.shared.getShop()?.id,
      'date': DateFormat('yyyy-MM-dd').format(currentDate.value)
    });
    if (resData.success == true && resData.data?.items?.isNotEmpty == true) {
      var listCategorized = resData.data!.items!
          .map((e) => CategorizedDayEvent<BookingModel>(
                categoryId: e.employee?.id?.toString() ?? '',
                value: e,
                start: setDateTimeTime(
                        e.date ?? DateTime.now(), e.startTime ?? '8:00') ??
                    DateTime.now(),
                end: setDateTimeTime(
                        e.date ?? DateTime.now(), e.endTime ?? '20:00') ??
                    DateTime.now(),
              ))
          .toList();

      listBooking.addAll(listCategorized);
      listBooking.refresh();
    }
  }

  genEvents(int length) {
    listBooking.clear();
  }

  void getEmployeeData() async {
    var resData =
        await _employeeProvider.getListEmployee({'all': 1, 'type': '3'});
    if (resData.success == true && resData.data?.items?.isNotEmpty == true) {
      listEmployeeCalendar.addAll(resData.data!.items!.map((e) =>
          EventCategory(id: e.id?.toString() ?? '', name: e.name ?? '')));
      listEmployeeCalendar.refresh();
    }
  }

  void updateBooking(BookingModel item, int status) async {
    EasyLoading.show();
    var result = await _bookingProvider.updateBooking(item.id ?? 0, {
      "status": status,
    });
    if (result.success == true && result.data != null) {
      var index =
          listBooking.indexWhere((item) => item.value.id == item.value.id);
      listBooking[index] = CategorizedDayEvent<BookingModel>(
        categoryId: result.data!.employee?.id?.toString() ?? '',
        value: result.data!,
        start: setDateTimeTime(result.data!.date ?? DateTime.now(),
                result.data!.startTime ?? '8:00') ??
            DateTime.now(),
        end: setDateTimeTime(result.data!.date ?? DateTime.now(),
                result.data!.endTime ?? '20:00') ??
            DateTime.now(),
      );
      listBooking.refresh();
    } else {
      _messageService.send(
          Message.error(content: result.message ?? 'update_booking_failed'.tr));
    }
    EasyLoading.dismiss();
  }

  Future<void> cancelBooking(BookingModel item) async {
    EasyLoading.show();
    var result = await _bookingProvider.deleteBooking(item.id ?? 0);
    if (result.success == true) {
      var index =
          listBooking.indexWhere((item) => item.value.id == item.value.id);
      listBooking.removeAt(index);
      listBooking.refresh();
    } else {
      _messageService.send(
          Message.error(content: result.message ?? 'cancel_booking_failed'.tr));
    }
    EasyLoading.dismiss();
  }
}
