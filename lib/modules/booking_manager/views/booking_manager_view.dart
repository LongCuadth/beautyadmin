// ignore_for_file: invalid_use_of_protected_member

import 'package:app/data/models/booking_model.dart';
import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/booking_manager/views/detail_booking_view.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/format_util.dart';
import 'package:app/utils/icon_circle_widget.dart';
import 'package:calendar_day_view/calendar_day_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../controllers/booking_manager_controller.dart';

class BookingManagerView extends GetView<BookingManagerController> {
  const BookingManagerView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      appBar: AppBar(
        elevation: 0,
        leading: InkWell(
          radius: 30.r,
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back_ios,
            color: ThemeProvider.colorBlack,
            size: 24,
          ),
        ),
        // actions: [
        // InkWell(
        //   radius: 30.r,
        //   onTap: () {
        //     Get.toNamed(Routes.APPOINTMENT_MANAGER);
        //   },
        //   child: Assets.svg.icAppointmentManager
        //       .svg(
        //           colorFilter: const ColorFilter.mode(
        //               ThemeProvider.colorBlack, BlendMode.srcIn))
        //       .marginOnly(right: 16.w),
        // ),
        // ],
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        title: Text('booking'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontLogoBold,
                fontSize: ThemeProvider.fontSize24,
                color: ThemeProvider.colorBlack)),
        centerTitle: true,
      ),
      body: Column(
        children: [
          GestureDetector(
            onTap: () async {
              var dateSelect = await showDatePicker(
                context: context,
                initialDate: controller.currentDate.value,
                firstDate: DateTime(2023),
                lastDate: DateTime(2100),
                builder: (context, child) {
                  return Theme(
                    data: Theme.of(context).copyWith(
                      colorScheme: const ColorScheme.light(
                        primary: ThemeProvider.colorPrimary,
                      ),
                      textButtonTheme: TextButtonThemeData(
                        style: TextButton.styleFrom(
                          foregroundColor:
                              ThemeProvider.colorPrimary, // button text color
                        ),
                      ),
                    ),
                    child: child!,
                  );
                },
              );
              if (dateSelect != null) {
                if (dateSelect != controller.currentDate.value) {
                  controller.currentDate.value = dateSelect;
                  controller.getBookingData();
                }
              }
            },
            child: Row(
              children: [
                Assets.svg.icCalendar.svg(),
                SizedBox(width: 10.w),
                Obx(
                  () => Text(
                    formatDate(
                        date: controller.currentDate.value,
                        formatDate: 'E, dd MMM yyyy'),
                    style: TextStyle(
                        fontFamily: ThemeProvider.fontMedium,
                        fontSize: ThemeProvider.fontSize16,
                        color: ThemeProvider.colorPrimary),
                  ),
                )
              ],
            ).marginOnly(left: 16.w, bottom: 10.h),
          ),
          Flexible(child: _buildCalender()),
        ],
      ),
    );
  }

  Widget _buildCalender() {
    return Obx(() {
      return controller.listBooking.isEmpty ||
              controller.listEmployeeCalendar.isEmpty
          ? Center(
              child: Text('empty'.tr),
            )
          : CalendarDayView<BookingModel>.categoryOverflow(
              logo: const SizedBox.shrink(),
              controlBarBuilder: (goToPreviousTab, goToNextTab) {
                return const SizedBox();
              },
              allowHorizontalScroll: true,
              categories: controller.listEmployeeCalendar.value.toList(),
              events: controller.listBooking.value.toList(),
              currentDate: controller.currentDate.value,
              categoryTextStyle: TextStyle(
                  color: ThemeProvider.colorTextBlack,
                  fontFamily: ThemeProvider.fontMedium,
                  fontSize: ThemeProvider.fontSize14),
              timeTextStyle: TextStyle(
                  fontFamily: ThemeProvider.fontRegular,
                  fontSize: ThemeProvider.fontSize14),
              timeGap: 30,
              onTileTap: (category, time, model) {
                if (model != null) {
                  _openAction(model.value as BookingModel);
                }
              },
              startOfDay: controller.startTimeOfDay.value,
              endOfDay: controller.endTimeOfDay.value,
              columnsPerPage: 2,
              heightPerMin: 1.5,
              timeColumnWidth: 80,
              evenRowColor: Colors.white,
              oddRowColor: Colors.white,
              headerDecoration: BoxDecoration(
                color: ThemeProvider.colorPrimary.withOpacity(.2),
              ),
              eventBuilder: (constraints, category, event, value) {
                return value != null
                    ? _buildEvent(constraints, value)
                    : const SizedBox.shrink();
              });
    });
  }

  Widget _buildEvent(
      BoxConstraints constraints, CategorizedDayEvent<BookingModel> value) {
    return Container(
        margin: const EdgeInsets.only(left: 10),
        padding: const EdgeInsets.all(4),
        width: constraints.maxWidth - 20,
        constraints: constraints,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: ThemeProvider.colorPrimary.withOpacity(0.3),
                blurRadius: 16,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ],
            color: getCourseStatusColor(value.value.status ?? 2),
            borderRadius: const BorderRadius.all(Radius.circular(16))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                    child: Text(
                  value.value.customer?.name ?? '',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      fontFamily: ThemeProvider.fontBold,
                      fontSize: ThemeProvider.fontSize14,
                      color: ThemeProvider.colorTextWhite),
                )),
                GestureDetector(
                    onTap: () {
                      // debugPrint('notification');
                    },
                    child: const SizedBox(
                      width: 26,
                      height: 26,
                      child: IconCircleWidget(
                          backgroundColor: Colors.red,
                          child: Icon(
                            Icons.notifications,
                            size: 16,
                            color: ThemeProvider.colorWhite,
                          )),
                    ))
              ],
            ),
            const SizedBox(height: 2),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                // const Flexible(
                //     flex: 1,
                //     child: SizedBox(
                //       width: 16,
                //       height: 16,
                //       child: IconCircleWidget(
                //           backgroundColor: Colors.white,
                //           child: Padding(
                //             padding: EdgeInsets.all(2),
                //             child: IconCircleWidget(
                //               backgroundColor: Colors.green,
                //               child: SizedBox(height: 0),
                //             ),
                //           )),
                //     )),
                Flexible(
                    flex: 4,
                    child: Text(
                      value.value.listProduct?.firstOrNull?.name ?? '',
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontSemiBold,
                          fontSize: ThemeProvider.fontSize12,
                          color: ThemeProvider.colorTextWhite),
                      overflow: TextOverflow.ellipsis,
                      maxLines: constraints.maxHeight > 60 ? 2 : 1,
                    )),
                Flexible(flex: 1, child: Assets.svg.icPaste.svg(height: 20))
              ],
            )
          ],
        ));
  }

  void _openAction(BookingModel item) {
    Get.bottomSheet(
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          decoration: const BoxDecoration(
            color: ThemeProvider.colorWhite,
          ),
          height: Get.height * 0.4,
          child: Column(
            children: [
              _buidAction(
                  icon: Icons.insert_drive_file_rounded,
                  value: 'see_detail'.tr,
                  onTap: () async {
                    Get.bottomSheet(
                        DetailBookingView(
                          bookingModel: item,
                        ),
                        isDismissible: true,
                        isScrollControlled: true);
                  }),
              // _buidAction(
              //     icon: Icons.edit_document,
              //     value: 'edit_booking'.tr,
              //     onTap: () async {
              //       Get.bottomSheet(
              //           DetailBookingView(
              //             bookingModel: item,
              //           ),
              //           isDismissible: true,
              //           isScrollControlled: true);
              //     }),
              Visibility(
                visible: (item.status ?? 1) < 2,
                child: _buidAction(
                    icon: Icons.bookmark_added,
                    value: 'approve_booking'.tr,
                    onTap: () async {
                      controller.updateBooking(item, 2);
                      Get.back();
                    }),
              ),
              Visibility(
                visible: (item.status ?? 1) < 3,
                child: _buidAction(
                    icon: Icons.fact_check_outlined,
                    value: 'checkin_booking'.tr,
                    onTap: () async {
                      controller.updateBooking(item, 3);
                      Get.back();
                    }),
              ),
              Visibility(
                visible: (item.status ?? 1) <= 3,
                child: _buidAction(
                    icon: Icons.payment,
                    value: 'payment_booking'.tr,
                    onTap: () async {
                      controller.updateBooking(item, 4);
                      Get.back();
                    }),
              ),
              Visibility(
                visible: (item.status ?? 1) != 4,
                child: _buidAction(
                    icon: Icons.cancel,
                    value: 'cancel_booking'.tr,
                    onTap: () {
                      controller.cancelBooking(item);
                      Get.back();
                    }),
              )
            ],
          ),
        ),
        isScrollControlled: true,
        isDismissible: true);
  }

  Widget _buidAction({IconData? icon, String? value, VoidCallback? onTap}) {
    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.h),
        child: Row(
          children: [
            Icon(
              icon,
              color: ThemeProvider.colorPrimary,
            ),
            SizedBox(width: 40.w),
            Text(
              value ?? '',
              style: TextStyle(
                  fontFamily: ThemeProvider.fontMedium,
                  fontSize: ThemeProvider.fontSize16),
            )
          ],
        ),
      ),
    );
  }
}
