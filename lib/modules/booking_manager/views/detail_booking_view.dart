import 'package:app/data/models/booking_model.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/format_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class DetailBookingView extends StatefulWidget {
  final BookingModel bookingModel;
  const DetailBookingView({super.key, required this.bookingModel});

  @override
  State<DetailBookingView> createState() => _DetailBookingViewState();
}

class _DetailBookingViewState extends State<DetailBookingView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ThemeProvider.colorWhite,
      height: Get.height * 0.5,
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(16),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Align(
            alignment: Alignment.topCenter,
            child: Text(
              'detail_booking'.tr,
              style: TextStyle(
                color: ThemeProvider.colorPrimary,
                fontFamily: ThemeProvider.fontSemiBold,
                fontSize: ThemeProvider.fontSize18,
              ),
            ),
          ),
          SizedBox(
            height: 20.h,
          ),
          Text(
            'info'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontMedium,
                fontSize: ThemeProvider.fontSize16,
                color: ThemeProvider.colorPrimary),
          ),
          SizedBox(
            height: 10.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  '${'customer'.tr}: ${widget.bookingModel.customer?.name}',
                  style: TextStyle(
                      fontFamily: ThemeProvider.fontRegular,
                      fontSize: ThemeProvider.fontSize14),
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.topRight,
                  child: Text(
                    '${'employee'.tr}: ${widget.bookingModel.employee?.name}',
                    style: TextStyle(
                        fontFamily: ThemeProvider.fontRegular,
                        fontSize: ThemeProvider.fontSize14),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.h,
          ),
          Text(
            '${'start_time'.tr}:    ${formatDate(date: setDateTimeTime(widget.bookingModel.date ?? DateTime.now(), widget.bookingModel.startTime ?? '8:00') ?? DateTime.now(), formatDate: 'EEE, d MMM yyyy HH:mm')}',
            style: TextStyle(
                fontFamily: ThemeProvider.fontRegular,
                fontSize: ThemeProvider.fontSize14),
          ),
          SizedBox(
            height: 10.h,
          ),
          Text(
            '${'end_time'.tr}:    ${formatDate(date: setDateTimeTime(widget.bookingModel.date ?? DateTime.now(), widget.bookingModel.endTime ?? '20:00') ?? DateTime.now(), formatDate: 'EEE, d MMM yyyy HH:mm')}',
            style: TextStyle(
                fontFamily: ThemeProvider.fontRegular,
                fontSize: ThemeProvider.fontSize14),
          ),
          SizedBox(
            height: 10.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '${'duration'.tr}: ${widget.bookingModel.duration}',
                style: TextStyle(
                    fontFamily: ThemeProvider.fontRegular,
                    fontSize: ThemeProvider.fontSize14),
              ),
              Text(
                '${'price'.tr}: ${widget.bookingModel.price} €',
                style: TextStyle(
                    fontFamily: ThemeProvider.fontRegular,
                    fontSize: ThemeProvider.fontSize14),
              ),
            ],
          ),
          SizedBox(
            height: 15.h,
          ),
          Row(
            children: [
              Text(
                'status'.tr,
                style: TextStyle(
                    fontFamily: ThemeProvider.fontMedium,
                    fontSize: ThemeProvider.fontSize16,
                    color: ThemeProvider.colorPrimary),
              ),
              SizedBox(
                width: 20.w,
              ),
              Text(
                getCourseStatusName(widget.bookingModel.status ?? 1),
                style: TextStyle(
                    fontFamily: ThemeProvider.fontRegular,
                    fontSize: ThemeProvider.fontSize14),
              )
            ],
          ),
          SizedBox(
            height: 15.h,
          ),
          Text(
            'services'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontMedium,
                fontSize: ThemeProvider.fontSize16,
                color: ThemeProvider.colorPrimary),
          ),
          SizedBox(
            height: 5.h,
          ),
          widget.bookingModel.listProduct != null &&
                  widget.bookingModel.listProduct!.isNotEmpty
              ? ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: widget.bookingModel.listProduct?.length ?? 0,
                  itemBuilder: (context, index) {
                    var item = widget.bookingModel.listProduct![index];
                    return Text(
                      item.name ?? '',
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontRegular,
                          fontSize: ThemeProvider.fontSize14,
                          color: ThemeProvider.colorBlack),
                    );
                  },
                )
              : const SizedBox(),
        ]),
      ),
    );
  }
}
