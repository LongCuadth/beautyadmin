import 'package:app/data/providers/banner/banner_provider.dart';
import 'package:get/get.dart';

import '../controllers/banner_manager_controller.dart';

class BannerManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IBannerProvider>(() => BannerProvider());
    Get.lazyPut<BannerManagerController>(
      () => BannerManagerController(),
    );
  }
}
