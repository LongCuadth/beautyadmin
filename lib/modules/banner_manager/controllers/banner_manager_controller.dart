import 'package:app/data/models/banner_model.dart';
import 'package:app/data/providers/banner/banner_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart' as dio;

class BannerManagerController extends GetxController {
  var listBanner = <BannerModel>[].obs;
  final IBannerProvider _bannerProvider = Get.find<IBannerProvider>();
  final MessageService _messageService = Get.find<MessageService>();
  final ImagePicker picker = ImagePicker();
  var filePath = ''.obs;

  @override
  void onInit() {
    _getBanner();
    super.onInit();
  }

  Future<void> selectImage() async {
    final path = await pickImage(ImageSource.gallery);
    if (path != null) filePath.value = path;
    if (path != null && path.isNotEmpty) {
      uploadImage();
    }
  }

  Future<String?> pickImage(ImageSource source) async {
    final XFile? photo =
        await picker.pickImage(source: source, imageQuality: 100);
    return photo?.path;
  }

  _getBanner() async {
    EasyLoading.show();
    var result = await _bannerProvider.getListBanner({'size': 1000});
    EasyLoading.dismiss();
    if (result.success == true && result.data!.items != null) {
      listBanner.value = (result.data!.items!);
    }
    listBanner.refresh();
  }

  uploadImage() async {
    EasyLoading.show();
    var result = await _bannerProvider.createBanner({
      'title': 'title',
      'type': 1,
      'image': await dio.MultipartFile.fromFile(filePath.value,
          filename: filePath.value.split('/').last)
    });
    EasyLoading.dismiss();
    if (result.success == true) {
      _getBanner();
    }
  }

  Future<void> deleteImage(int index) async {
    EasyLoading.show();
    var result = await _bannerProvider.deleteBanner(listBanner[index].id ?? 0);
    EasyLoading.dismiss();
    if (result.success == true) {
      _getBanner();
    }
  }
}
