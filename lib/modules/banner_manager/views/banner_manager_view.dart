import 'package:app/themes/themes_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/banner_manager_controller.dart';

class BannerManagerView extends GetView<BannerManagerController> {
  const BannerManagerView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        appBar: AppBar(
          elevation: 0,
          leading: InkWell(
            radius: 30.r,
            onTap: () {
              Get.back();
            },
            child: const Icon(
              Icons.arrow_back_ios,
              color: ThemeProvider.colorBlack,
              size: 24,
            ),
          ),
          backgroundColor: ThemeProvider.colorBackgroundScreen,
          title: Text('banner_manager'.tr,
              style: TextStyle(
                  fontFamily: ThemeProvider.fontLogoBold,
                  fontSize: ThemeProvider.fontSize24,
                  color: ThemeProvider.colorBlack)),
          centerTitle: true,
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: ThemeProvider.colorPrimary,
          onPressed: () {
            controller.selectImage();
          },
          child: const Icon(Icons.add),
        ),
        body: Obx(() => controller.listBanner.isNotEmpty
            ? _buildListBanner()
            : Center(
                child: Text('empty'.tr),
              )));
  }

  Widget _buildListBanner() {
    return ListView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: controller.listBanner.length,
        itemBuilder: (context, index) {
          return Container(
            margin: const EdgeInsets.symmetric(vertical: 10),
            width: Get.width,
            height: 120,
            child: Row(
              children: [
                Expanded(
                  child: CachedNetworkImage(
                    imageUrl: controller.listBanner[index].image ?? '',
                    fit: BoxFit.fill,
                  ),
                ),
                const SizedBox(width: 20),
                GestureDetector(
                  onTap: () {
                    controller.deleteImage(index);
                  },
                  child: Container(
                      width: 40,
                      decoration: BoxDecoration(
                          color: Colors.grey.shade200,
                          borderRadius: BorderRadius.circular(10)),
                      child: const Center(
                          child: Icon(
                        Icons.delete,
                        color: Colors.red,
                      ))),
                ),
                const SizedBox(width: 20),
              ],
            ),
          );
        });
  }
}
