import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/modules/forgot_password/controllers/forgot_password_controller.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/icon_circle_widget.dart';
import 'package:app/utils/validator_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

class ForgotPasswordView extends GetView<ForgotPasswordController> {
  const ForgotPasswordView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      body: SafeArea(child: _buildFormForgot()),
    );
  }

  Widget _buildFormForgot() {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 16.w),
      child: Form(
        key: controller.keyForm,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: IconCircleWidget(
                padding: EdgeInsets.only(left: 8.w),
                backgroundColor: ThemeProvider.colorPrimary.withOpacity(0.2),
                child: const Icon(
                  Icons.arrow_back_ios,
                  color: ThemeProvider.colorBlack,
                  size: 22,
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: RichText(
                text: TextSpan(
                    text: 'app_name'.tr,
                    style: TextStyle(
                        color: ThemeProvider.colorBlack,
                        fontFamily: ThemeProvider.fontLogoBold,
                        fontSize: ThemeProvider.fontSize28),
                    children: [
                      TextSpan(
                          text: 'admin'.tr,
                          style: TextStyle(
                              color: ThemeProvider.colorPolicy,
                              fontFamily: ThemeProvider.fontLogoBold,
                              fontSize: ThemeProvider.fontSize28)),
                    ]),
              ),
            ),
            SizedBox(height: 150.h),
            buildTextField().marginSymmetric(horizontal: 16.w),
            SizedBox(height: 25.h),
            BaseButton(
                radius: 4,
                title: 'confirm'.tr,
                onPressed: () {
                  if (controller.isPasswordVerified.value == false) {
                    controller.checkPhone();
                  } else {
                    controller.changePassword();
                  }
                },
                styleButton: BaseButtonStyle.fill),
          ],
        ),
      ),
    );
  }

  Widget buildTextField() {
    return Obx(() => controller.isPasswordVerified.value
        ? Column(
            children: [
              BaseTextField(
                  validator: (value) {
                    return ValidatorUtils.validatorPass(value);
                  },
                  isPassword: true,
                  inputType: TextInputType.text,
                  textEditingController: controller.newPassController,
                  hintText: 'password_new'.tr),
              SizedBox(height: 25.h),
              BaseTextField(
                  validator: (value) {
                    return ValidatorUtils.validatorRePass(
                        controller.newPassController.value.text, value);
                  },
                  isPassword: true,
                  inputType: TextInputType.text,
                  textEditingController: controller.checkPassController,
                  hintText: 'password_check'.tr),
            ],
          )
        : BaseTextField(
            autoFocus: true,
            validator: (value) {
              return ValidatorUtils.validatorPhone(value);
            },
            inputFormatters: [
              LengthLimitingTextInputFormatter(12),
            ],
            inputType: TextInputType.phone,
            textEditingController: controller.phoneController,
            hintText: 'phone'.tr));
  }
}
