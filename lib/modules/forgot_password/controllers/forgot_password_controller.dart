import 'package:app/common/app_parameters.dart';
import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ForgotPasswordController extends GetxController {
  var phoneController = TextEditingController();
  var newPassController = TextEditingController();
  var checkPassController = TextEditingController();
  var isPasswordVerified = false.obs;
  var keyForm = GlobalKey<FormState>();
  final IAuthProvider _authProvider = Get.find<IAuthProvider>();
  final MessageService _messageService = Get.find<MessageService>();

  Future<void> checkPhone() async {
    if (keyForm.currentState!.validate()) {
      EasyLoading.show();
      var res = await _authProvider
          .checkPhoneExits({AppParameters.phoneNumber: phoneController.text});
      EasyLoading.dismiss();
      if (res.success == true) {
        isPasswordVerified.value = true;
      } else {
        _messageService.send(Message.error(content: 'user_not_exits'.tr));
      }
    }
  }

  void changePassword() {
    if (keyForm.currentState!.validate()) {
      isPasswordVerified.value = false;
    }
  }
}
