import 'package:app/data/models/category_model.dart';
import 'package:app/data/models/services_model.dart';
import 'package:app/data/providers/service/service_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ServicesManagerController extends GetxController {
  var textEditingController = TextEditingController();
  var listServices = <ServicesModel>[].obs;
  var listSearch = <ServicesModel>[].obs;
  var listCategory = <CategoryModel>[].obs;
  var scrollController = ScrollController();
  var page = 1;
  final GlobalKey<ScaffoldState> scaffoldkey = GlobalKey();
  final IServicesProvider _serviceProvider = Get.find<IServicesProvider>();
  final MessageService _messageService = Get.find<MessageService>();
  var minValue = 0.0.obs;
  var maxValue = 100000000.0.obs;
  var canLoadmore = true.obs;

  @override
  void onInit() {
    getServices();
    super.onInit();
    listSearch.assignAll(listServices);
  }

  void getServices({bool isLoadMore = false}) async {
    EasyLoading.show();
    var res = await _serviceProvider.getListService({
      'size': '10',
      'page': page,
      'category_id': 1,
      'keyword': textEditingController.text,
      'from': minValue.value,
      'to': maxValue.value
    });

    if (res.success == true && res.data != null) {
      if (isLoadMore && res.data?.items?.isEmpty == true) {
        canLoadmore.value = false;
      } else {
        listServices.addAll(res.data?.items ?? []);
        listSearch.assignAll(listServices);
      }
    }
    EasyLoading.dismiss();
  }

  void loadData() {
    canLoadmore.value = true;
    page = 1;
    listSearch.clear();
    listServices.clear();
    getServices();
  }

  void onSearch(String value) {
    Future.delayed(const Duration(milliseconds: 400), () {
      loadData();
    });
  }

  void onRefresh() {
    loadData();
  }

  void onFilterPrice() {
    loadData();
  }

  void loadMore() {
    if (!canLoadmore.value) {
      return;
    }
    page += 1;
    getServices(isLoadMore: true);
  }

  void deleteItem(ServicesModel item) async {
    EasyLoading.show();
    var response = await _serviceProvider.deleteService(item.id ?? 0);
    EasyLoading.dismiss();
    if (response.success == true) {
      listSearch.removeWhere((element) => element.id == item.id);
      listServices.removeWhere((element) => element.id == item.id);
      listSearch.refresh();
      _messageService
          .send(Message.success(content: 'delete_service_success'.tr));
    } else {
      _messageService
          .send(Message.success(content: 'delete_service_failed'.tr));
    }
  }
}
