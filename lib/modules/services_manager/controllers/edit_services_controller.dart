import 'package:app/common/app_parameters.dart';
import 'package:app/data/models/category_model.dart';
import 'package:app/data/models/services_model.dart';
import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/providers/service/service_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../../data/models/employee_model.dart';
import '../../../data/services/message_service.dart';

class EditServicesController extends GetxController {
  var nameController = TextEditingController();
  var detailController = TextEditingController();
  var priceController = TextEditingController();
  var minutesController = TextEditingController();
  var selectedCategory = Rx<CategoryModel?>(null);
  var listEmployee = <EmployeeModel>[].obs;
  var listEmployeeSelected = <EmployeeModel>[].obs;
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _messageService = Get.find<MessageService>();
  var serviceModel = ServicesModel();
  var listCategory = <CategoryModel>[].obs;

  final IServicesProvider _serviceProvider = Get.find<IServicesProvider>();
  final ICategoryProvider _categoryProvider = Get.find<ICategoryProvider>();
  final IEmployeeProvider _employeeProvider = Get.find<IEmployeeProvider>();

  @override
  void onInit() {
    getEmployee();

    super.onInit();
  }

  void _bindingData() {
    serviceModel = Get.arguments[AppParameters.serviceModel] as ServicesModel;
    nameController.text = serviceModel.name ?? '';
    detailController.text = serviceModel.description ?? 'temp data';
    priceController.text = serviceModel.price ?? '20';
    minutesController.text = serviceModel.duration?.toString() ?? "20";
    selectedCategory.value = listCategory
        .firstWhere((element) => element.id == serviceModel.idCategory);
    serviceModel.employee?.forEach((itemSelect) {
      for (var itemEmployee in listEmployee) {
        if (itemSelect == itemEmployee.id) {
          listEmployeeSelected.add(itemEmployee);
        }
      }
    });
  }

  Future<void> getEmployee() async {
    // get Category
    var resCategory =
        await _categoryProvider.getListCategory({'all': 1, 'category_id': '1'});
    if (resCategory.success == true && resCategory.data != null) {
      listCategory.addAll(resCategory.data!.items ?? []);
    }
    // get Employee
    var resCustomer =
        await _employeeProvider.getListEmployee({'all': 1, 'type': '3'});
    listEmployee.addAll(resCustomer.data?.items ?? []);
    _bindingData();
  }

  void updateServices() async {
    if (formKey.currentState!.validate()) {
      if (selectedCategory.value != null) {
        serviceModel.description = detailController.text;
        serviceModel.name = nameController.text;
        serviceModel.price = priceController.text;
        serviceModel.duration = int.parse(minutesController.text);
        serviceModel.idCategory = selectedCategory.value?.id ?? 1;
        serviceModel.employee =
            listEmployeeSelected.map((element) => element.id ?? 0).toList();
        EasyLoading.show();
        var payload = serviceModel.toCreateModelJson();
        var response = await _serviceProvider.updateServices(
            serviceModel.id ?? 0, payload);
        EasyLoading.dismiss();
        if (response.success == false) {
          _messageService
              .send(Message.error(content: 'update_service_failed'.tr));
        } else {
          _messageService
              .send(Message.success(content: 'update_service_success'.tr));
          Get.back(result: response.data);
        }
      } else {
        _messageService
            .send(Message.error(content: 'category_not_selected'.tr));
      }
    }
  }
}
