import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_dropdown_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/data/models/category_model.dart';
import 'package:app/data/models/employee_model.dart';
import 'package:app/modules/services_manager/controllers/edit_services_controller.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class EditServicesView extends GetView<EditServicesController> {
  const EditServicesView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        appBar: AppBar(
          elevation: 0,
          leading: InkWell(
            radius: 30.r,
            onTap: () {
              Get.back();
            },
            child: const Icon(
              Icons.arrow_back_ios,
              color: ThemeProvider.colorBlack,
              size: 24,
            ),
          ),
          backgroundColor: ThemeProvider.colorBackgroundScreen,
          title: Text('edit_services'.tr,
              style: TextStyle(
                  fontFamily: ThemeProvider.fontLogoBold,
                  fontSize: ThemeProvider.fontSize24,
                  color: ThemeProvider.colorBlack)),
          centerTitle: true,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
                child: Form(
              key: controller.formKey,
              child: SingleChildScrollView(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 15.h),
                    Text(
                      'info_services'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    BaseTextField(
                        inputType: TextInputType.name,
                        textEditingController: controller.nameController,
                        validator: (value) {
                          if (value?.isEmpty == true) {
                            return 'empty_name_category'.tr;
                          }
                          return null;
                        },
                        hintText: 'name_services'.tr),
                    SizedBox(height: 20.h),
                    BaseTextField(
                        inputType: TextInputType.text,
                        textEditingController: controller.detailController,
                        hintText: 'detail_services'.tr),
                    SizedBox(height: 20.h),
                    Row(
                      children: [
                        Expanded(
                          child: BaseTextField(
                              inputType: TextInputType.number,
                              suffixIcon: const Icon(
                                Icons.euro,
                                size: 14,
                                color: ThemeProvider.colorSelect,
                              ),
                              textEditingController: controller.priceController,
                              validator: (value) {
                                if (value?.isEmpty == true) {
                                  return 'empty_price_category'.tr;
                                }
                                return null;
                              },
                              hintText: 'price'.tr),
                        ),
                        SizedBox(width: 40.w),
                        Expanded(
                          child: BaseTextField(
                              inputType: TextInputType.number,
                              suffixIcon: Text('minutes'.tr,
                                  style: TextStyle(
                                    fontFamily: ThemeProvider.fontRegular,
                                    fontSize: ThemeProvider.fontSize10,
                                    color: ThemeProvider.colorSelect,
                                  )),
                              textEditingController:
                                  controller.minutesController,
                              validator: (value) {
                                if (value?.isEmpty == true) {
                                  return 'empty_duration_category'.tr;
                                }
                                return null;
                              },
                              hintText: 'duration'.tr),
                        ),
                      ],
                    ),
                    SizedBox(height: 30.h),
                    Text(
                      'category'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    SizedBox(height: 10.h),
                    _buildSelectCategory(),
                    SizedBox(height: 30.h),
                    Text(
                      'employee'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    SizedBox(height: 10.h),
                    _buildSelectEmployee(),
                    SizedBox(height: 20.h),
                    _buildListEmployeeSelected(),
                    SizedBox(height: 20.h),
                  ],
                ),
              ),
            )),
            BaseButton(
                    title: 'save'.tr,
                    onPressed: () {
                      controller.updateServices();
                    },
                    styleButton: BaseButtonStyle.fill)
                .marginSymmetric(horizontal: 16.w, vertical: 20.h)
          ],
        ));
  }

  _buildSelectCategory() {
    return Obx(
      () => BaseDropdownButton<CategoryModel>(
          buttonWidth: double.maxFinite,
          borderRadiusButton: 5,
          dropdownWidth: Get.width - 16.w * 2,
          dropdownElevation: 2,
          hint: 'hint_select_category'.tr,
          itemViewString: (item) {
            return item.name ?? '';
          },
          value: controller.selectedCategory.value,
          leadingHint: const Icon(
            Icons.spa_outlined,
            color: ThemeProvider.colorPrimary,
            size: 24,
          ).marginOnly(right: 15.w),
          icon: const Icon(
            Icons.keyboard_arrow_down_outlined,
            size: 24,
          ),
          hintStyle: TextStyle(
              fontFamily: ThemeProvider.fontRegular,
              fontSize: ThemeProvider.fontSize14,
              color: ThemeProvider.colorHintText),
          textStyle: TextStyle(
              fontFamily: ThemeProvider.fontRegular,
              fontSize: ThemeProvider.fontSize14,
              color: ThemeProvider.colorBlack),
          dropdownItems: controller.listCategory.toList(),
          onChanged: (value) {
            if (value != null) {
              controller.selectedCategory.value = value;
            }
          }),
    );
  }

  _buildSelectEmployee() {
    return GestureDetector(
      onTap: () {
        Get.bottomSheet(
          _selectEmployeeBottomSheet(),
          isScrollControlled: true,
          isDismissible: true,
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 10.h),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(width: 0.5)),
        width: double.maxFinite,
        child: Text(
          'select_employee'.tr,
          style: TextStyle(
              fontFamily: ThemeProvider.fontRegular,
              fontSize: ThemeProvider.fontSize14),
        ),
      ),
    );
  }

  Widget _buildListEmployeeSelected() {
    return Obx(() => controller.listEmployeeSelected.isNotEmpty
        ? ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: controller.listEmployeeSelected.length,
            itemBuilder: (context, index) {
              var item = controller.listEmployeeSelected[index];
              return ListTile(
                title: Text(
                  item.name ?? '',
                  style: TextStyle(
                      fontFamily: ThemeProvider.fontMedium,
                      fontSize: ThemeProvider.fontSize14),
                ),
                trailing: GestureDetector(
                  onTap: () {
                    controller.listEmployeeSelected.remove(item);
                    controller.listEmployee.refresh();
                  },
                  child: const Icon(Icons.delete_outline,
                      color: ThemeProvider.colorSelect),
                ),
              );
            },
          )
        : Text('empty_employee_select'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontMedium,
                fontSize: ThemeProvider.fontSize14,
                color: ThemeProvider.colorSelect)));
  }

  Widget _selectEmployeeBottomSheet() {
    return Container(
      padding: EdgeInsets.only(bottom: 10.h),
      color: ThemeProvider.colorWhite,
      child: MultiSelectBottomSheet<EmployeeModel>(
        items: controller.listEmployee
            .map((element) =>
                MultiSelectItem<EmployeeModel>(element, element.name ?? ''))
            .toList(),
        initialValue: controller.listEmployeeSelected,
        onConfirm: (values) {
          controller.listEmployeeSelected.clear();
          controller.listEmployeeSelected.addAll(values);
        },
        maxChildSize: 0.7,
        title: Text('select_employee'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontSemiBold,
                fontSize: ThemeProvider.fontSize16,
                color: ThemeProvider.colorPrimary)),
        cancelText: Text('cancel'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontSemiBold,
                fontSize: ThemeProvider.fontSize16,
                color: ThemeProvider.colorBlack)),
        confirmText: Text('confirm'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontSemiBold,
                fontSize: ThemeProvider.fontSize16,
                color: ThemeProvider.colorPrimary)),
        selectedColor: ThemeProvider.colorPrimary,
        itemsTextStyle: TextStyle(
            fontFamily: ThemeProvider.fontRegular,
            fontSize: ThemeProvider.fontSize18,
            color: ThemeProvider.colorBlack),
        selectedItemsTextStyle: TextStyle(
            fontFamily: ThemeProvider.fontRegular,
            fontSize: ThemeProvider.fontSize18,
            color: ThemeProvider.colorPrimary),
      ),
    );
  }
}
