import 'package:app/base/widgets/base_button.dart';
import 'package:app/modules/services_manager/controllers/services_manager_controller.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/format_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';

class ServiceFilterWidget extends StatelessWidget {
  const ServiceFilterWidget({
    super.key,
    required this.controller,
  });

  final ServicesManagerController controller;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: Get.width * 0.75,
      child: Drawer(
        elevation: 10,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: kToolbarHeight),
            Align(
              child: Text(
                'filter_services'.tr,
                style: TextStyle(
                    fontFamily: ThemeProvider.fontBold,
                    fontSize: ThemeProvider.fontSize18),
              ),
            ),
            SizedBox(height: 40.h),
            Text(
              'filter_price'.tr,
              style: TextStyle(
                  fontFamily: ThemeProvider.fontBold,
                  fontSize: ThemeProvider.fontSize14),
            ).marginOnly(left: 16.w),
            SizedBox(height: 20.h),
            Obx(() {
              var range = RangeValues(
                  controller.minValue.value, controller.maxValue.value);
              return RangeSlider(
                values: range,
                max: 100000000,
                min: 0.0,
                labels: RangeLabels(
                  range.start.round().toString(),
                  range.end.round().toString(),
                ),
                divisions: 10000000,
                onChanged: (RangeValues values) {
                  controller.minValue.value = values.start;
                  controller.maxValue.value = values.end;
                },
              );
            }),
            Obx(() => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Min: ${formatCurrency(controller.minValue.value.round().toDouble(), "€")}',
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontRegular,
                          fontSize: ThemeProvider.fontSize12),
                    ),
                    Text(
                      'Max: ${formatCurrency(controller.maxValue.value.round().toDouble(), "€")}',
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontRegular,
                          fontSize: ThemeProvider.fontSize12),
                    ),
                  ],
                ).marginSymmetric(horizontal: 16.w)),
            SizedBox(height: 40.h),
            // Text(
            //   'filter_category'.tr,
            //   style: TextStyle(
            //       fontFamily: ThemeProvider.fontBold,
            //       fontSize: ThemeProvider.fontSize14),
            // ).marginOnly(left: 16.w),
            Flexible(
                fit: FlexFit.tight,
                child: Obx(
                  () => ListView.builder(
                      itemCount: controller.listCategory.length,
                      itemBuilder: (context, index) {
                        var item = controller.listCategory[index];
                        return Container(
                          margin: EdgeInsets.only(bottom: 20.h),
                          padding: EdgeInsets.symmetric(vertical: 10.h),
                          decoration: const BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                      width: 0.2,
                                      color: ThemeProvider
                                          .borderButtonOutlineDisable))),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Text(
                                    item.name ?? '',
                                    style: TextStyle(
                                        fontFamily: ThemeProvider.fontMedium,
                                        fontSize: ThemeProvider.fontSize16),
                                    maxLines: 3,
                                  ).marginSymmetric(horizontal: 10.w)),
                              Expanded(
                                child: FlutterSwitch(
                                  activeColor: ThemeProvider.colorPrimary,
                                  height: 30.h,
                                  toggleSize: 20.0,
                                  value: item.isSelected,
                                  borderRadius: 30.0,
                                  padding: 4.0,
                                  onToggle: (val) {
                                    item.isSelected = val;
                                    controller.listCategory.refresh();
                                    controller.onSearch(
                                        controller.textEditingController.text);
                                  },
                                ).marginSymmetric(horizontal: 10.w),
                              )
                            ],
                          ),
                        );
                      }),
                )),
            BaseButton(
                    title: 'apply'.tr,
                    onPressed: () {
                      controller.onFilterPrice();
                    },
                    styleButton: BaseButtonStyle.fill)
                .marginOnly(bottom: 20.h)
          ],
        ),
      ),
    );
  }
}
