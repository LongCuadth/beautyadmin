import 'package:app/common/app_parameters.dart';
import 'package:app/data/models/services_model.dart';
import 'package:app/main.dart';
import 'package:app/modules/services_manager/views/service_filter_view.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/format_util.dart';
import 'package:app/utils/search_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:grouped_list/grouped_list.dart';

import '../controllers/services_manager_controller.dart';

class ServicesManagerView extends GetView<ServicesManagerController> {
  const ServicesManagerView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEnableOpenDragGesture: false,
      endDrawerEnableOpenDragGesture: false,
      key: controller.scaffoldkey,
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      appBar: AppBar(
        elevation: 0,
        leading: InkWell(
          radius: 30.r,
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back_ios,
            color: ThemeProvider.colorBlack,
            size: 24,
          ),
        ),
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        title: Text('services'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontLogoBold,
                fontSize: ThemeProvider.fontSize24,
                color: ThemeProvider.colorBlack)),
        centerTitle: true,
      ),
      endDrawer: _buildEndDrawer(context),
      body: Column(children: [
        SearchWidget(
          hintText: 'search_services'.tr,
          textEditingController: controller.textEditingController,
          onAddTap: () {
            Get.toNamed(Routes.ADD_SERVICE)?.then((value) {
              controller.onRefresh();
            });
          },
          onFilterTap: () {
            controller.scaffoldkey.currentState!.openEndDrawer();
          },
          onChanged: (value) {
            controller.onSearch(value);
          },
        ),
        SizedBox(height: 20.h),
        Expanded(
          child: NotificationListener(
            onNotification: (notification) {
              if (notification is ScrollEndNotification) {
                if (controller.scrollController.position.extentAfter == 0 &&
                    controller.listSearch.isNotEmpty) {
                  controller.loadMore();
                }
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: () async {
                controller.onRefresh();
              },
              child: SingleChildScrollView(
                  controller: controller.scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(vertical: 20.h),
                  child: _buildListResult()),
            ),
          ),
        ),
      ]),
    );
  }

  Widget _buildListResult() {
    return Obx(() => controller.listSearch.isNotEmpty
        ? GroupedListView<ServicesModel, String>(
            groupBy: (element) {
              return '';
            },
            // groupSeparatorBuilder: (value) {
            //   String? name = controller.listCategory
            //           .firstWhereOrNull((element) => element.id == value)
            //           ?.name ??
            //       '';
            //   return Container(
            //       padding: const EdgeInsets.all(16),
            //       color: ThemeProvider.colorPrimary.withOpacity(0.1),
            //       child: Text(
            //         name,
            //         style: TextStyle(
            //             fontFamily: ThemeProvider.fontBold,
            //             fontSize: ThemeProvider.fontSize16),
            //       ));
            // },
            groupSeparatorBuilder: (value) => const SizedBox.shrink(),
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            elements: controller.listSearch,
            itemBuilder: (context, item) {
              return InkWell(
                onTap: () {
                  _openAction(item);
                },
                child: Container(
                  decoration: const BoxDecoration(
                      border: Border(bottom: BorderSide(width: 0.2))),
                  margin: EdgeInsets.symmetric(
                    horizontal: 20.w,
                  ),
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        item.name ?? '',
                        style: TextStyle(
                            fontFamily: ThemeProvider.fontSemiBold,
                            fontSize: ThemeProvider.fontSize16),
                      ),
                      SizedBox(height: 5.h),
                      Text(
                        item.description ?? '',
                        style: TextStyle(
                            fontFamily: ThemeProvider.fontRegular,
                            fontSize: ThemeProvider.fontSize14),
                      ),
                      SizedBox(height: 5.h),
                      Text(
                        '${formatCurrency(double.parse(item.price ?? ''), '€')} / ${item.duration} mins',
                        style: TextStyle(
                            fontFamily: ThemeProvider.fontMedium,
                            fontSize: ThemeProvider.fontSize12,
                            color: ThemeProvider.colorSelect),
                      ),
                    ],
                  ),
                ),
              );
            })
        : Center(
            child: Text('empty'.tr),
          ).marginOnly(top: Get.height / 3));
  }

  _buildEndDrawer(BuildContext context) {
    return ServiceFilterWidget(controller: controller);
  }

  void _openAction(ServicesModel item) {
    Get.bottomSheet(
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          decoration: const BoxDecoration(
            color: ThemeProvider.colorWhite,
          ),
          height: Get.height * 0.25,
          child: Column(
            children: [
              _buidAction(
                  icon: Icons.edit_document,
                  value: 'edit_services'.tr,
                  onTap: () async {
                    Get.toNamed(Routes.EDIT_SERVICE, arguments: {
                      AppParameters.serviceModel: item,
                    })?.then((value) {
                      if (value != null && value is ServicesModel) {
                        var index = controller.listSearch
                            .indexWhere((element) => element.id == value.id);
                        if (index >= 0) {
                          controller.listSearch[index] = value;
                          controller.listSearch.refresh();
                          Navigator.of(navigatorKey.currentContext!).pop();
                        }
                      }
                    });
                  }),
              _buidAction(
                  icon: Icons.delete_forever_outlined,
                  value: 'delete_services'.tr,
                  onTap: () {
                    controller.deleteItem(item);
                    Get.back();
                  })
            ],
          ),
        ),
        isScrollControlled: true,
        isDismissible: true);
  }

  _buidAction({IconData? icon, String? value, VoidCallback? onTap}) {
    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.h),
        child: Row(
          children: [
            Icon(
              icon,
              color: ThemeProvider.colorPrimary,
            ),
            SizedBox(width: 40.w),
            Text(
              value ?? '',
              style: TextStyle(
                  fontFamily: ThemeProvider.fontMedium,
                  fontSize: ThemeProvider.fontSize16),
            )
          ],
        ),
      ),
    );
  }
}
