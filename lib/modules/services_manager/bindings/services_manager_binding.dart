import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/providers/service/service_provider.dart';
import 'package:get/get.dart';

import '../controllers/services_manager_controller.dart';

class ServicesManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IServicesProvider>(() => ServicesProvider(), fenix: true);
    Get.lazyPut<ICategoryProvider>(() => CategoryProvider(), fenix: true);
    Get.lazyPut<IEmployeeProvider>(() => EmployeeProvider(), fenix: true);
    Get.lazyPut<ServicesManagerController>(
      () => ServicesManagerController(),
    );
  }
}
