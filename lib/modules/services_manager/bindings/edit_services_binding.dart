import 'package:app/modules/services_manager/controllers/edit_services_controller.dart';
import 'package:get/get.dart';

class EditServicesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EditServicesController>(
      () => EditServicesController(),
    );
  }
}
