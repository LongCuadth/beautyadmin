import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class RegisterController extends GetxController {
  var formKey = GlobalKey<FormState>();
  var phoneController = TextEditingController();
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var passsController = TextEditingController();
  var checkPassController = TextEditingController();
  var inviteController = TextEditingController();
  final ImagePicker picker = ImagePicker();
  var avatarPath = ''.obs;

  Future<void> selectImage() async {
    final path = await pickImage(ImageSource.gallery);
    if (path != null) avatarPath.value = path;
  }

  Future<String?> pickImage(ImageSource source) async {
    final XFile? photo =
        await picker.pickImage(source: source, imageQuality: 100);
    return photo?.path;
  }

  void saveInfo() {
    if (formKey.currentState!.validate()) {
      // TODO call api register user

      // Get.back();
      // Get.back();
      // var mess = Get.find<MessageService>();
      // mess.send(const Message.success(content: 'Đăng ký thành công'));
    }
  }
}
