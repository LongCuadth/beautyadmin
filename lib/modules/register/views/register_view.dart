import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/icon_circle_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/register_controller.dart';

class RegisterView extends GetView<RegisterController> {
  const RegisterView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        // appBar: BaseAppBar(
        //   onBackPress: () {
        //     Get.back();
        //   },
        //   title: 'register'.tr,
        //   isShowBackButton: true,
        //   widgetRight: Center(
        //     child: BaseButton(
        //         title: 'save'.tr,
        //         textStyle: TextStyle(
        //             color: ThemeProvider.colorWhite,
        //             fontFamily: ThemeProvider.fontMedium,
        //             fontSize: ThemeProvider.fontSize16),
        //         onPressed: () {
        //           controller.saveInfo();
        //         },
        //         styleButton: BaseButtonStyle.textOnly),
        //   ).marginOnly(right: 8.w),
        // ),
        body: SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 16.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: IconCircleWidget(
                    padding: EdgeInsets.only(left: 8.w),
                    backgroundColor:
                        ThemeProvider.colorPrimary.withOpacity(0.2),
                    child: const Icon(
                      Icons.arrow_back_ios,
                      color: ThemeProvider.colorBlack,
                      size: 22,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: RichText(
                    text: TextSpan(
                        text: 'app_name'.tr,
                        style: TextStyle(
                            color: ThemeProvider.colorBlack,
                            fontFamily: ThemeProvider.fontLogoBold,
                            fontSize: ThemeProvider.fontSize28),
                        children: [
                          TextSpan(
                              text: 'admin'.tr,
                              style: TextStyle(
                                  color: ThemeProvider.colorPolicy,
                                  fontFamily: ThemeProvider.fontLogoBold,
                                  fontSize: ThemeProvider.fontSize28)),
                        ]),
                  ),
                ),
                SizedBox(
                  width: Get.width,
                  height: 80.h,
                ),
                // GestureDetector(
                //   onTap: () {
                //     controller.selectImage();
                //   },
                //   child: Obx(() => controller.avatarPath.isEmpty
                //       ? const CircleAvatar(
                //           backgroundColor: Colors.blueGrey,
                //           maxRadius: 60,
                //           child: Padding(
                //             padding: EdgeInsets.all(8),
                //             child: Icon(
                //               Icons.wallpaper_rounded,
                //               size: 40,
                //               color: ThemeProvider.colorWhite,
                //             ),
                //           ),
                //         )
                //       : CircleAvatarWidget(
                //           url: controller.avatarPath.value,
                //           size: 120,
                //         )),
                // ),
                // SizedBox(height: 30.h),
                buildFormUserInfo()
              ],
            ),
          ),
        ));
  }

  Widget buildFormUserInfo() {
    return Form(
        key: controller.formKey,
        child: Column(
          children: [
            IgnorePointer(
              ignoring: false,
              child: BaseTextField(
                textEditingController: controller.phoneController,
                hintText: 'phone'.tr,
              ),
            ),
            SizedBox(height: 25.h),
            BaseTextField(
              textEditingController: controller.nameController,
              hintText: 'name'.tr,
              validator: (value) {
                if (controller.nameController.value.text.isEmpty) {
                  return 'empty_name'.tr;
                }
                if (!RegExp(r'^.{6,}$')
                    .hasMatch(controller.nameController.value.text)) {
                  return 'invalid_name'.tr;
                }
                return null;
              },
            ),
            SizedBox(height: 25.h),
            // BaseTextField(
            //   inputType: TextInputType.emailAddress,
            //   textEditingController: controller.emailController,
            //   hintText: 'email'.tr,
            //   validator: (value) {
            //     if (controller.emailController.value.text.isEmpty) {
            //       return null;
            //     }
            //     if (!GetUtils.isEmail(controller.emailController.value.text)) {
            //       return 'invalid_email'.tr;
            //     }
            //     return null;
            //   },
            // ),
            // SizedBox(height: 25.h),
            BaseTextField(
              isPassword: true,
              textEditingController: controller.passsController,
              hintText: 'password'.tr,
              validator: (value) {
                if (controller.passsController.value.text.isEmpty) {
                  return 'empty_pass'.tr;
                }
                if (!RegExp(r'^.{6,}$')
                    .hasMatch(controller.passsController.value.text)) {
                  return 'invalid_pass'.tr;
                }
                return null;
              },
            ),
            SizedBox(height: 25.h),
            BaseTextField(
              isPassword: true,
              textEditingController: controller.checkPassController,
              hintText: 'password_check'.tr,
              validator: (value) {
                if (controller.checkPassController.value.text.isEmpty) {
                  return 'empty_pass'.tr;
                }
                if (!RegExp(r'^.{6,}$')
                    .hasMatch(controller.checkPassController.value.text)) {
                  return 'invalid_pass'.tr;
                }
                if (controller.checkPassController.value.text !=
                    controller.passsController.value.text) {
                  return 'pass_not_match'.tr;
                }
                return null;
              },
            ),
            SizedBox(height: 25.h),
            // BaseTextField(
            //   textEditingController: controller.inviteController,
            //   hintText: 'invite'.tr,
            // ),
            SizedBox(height: 25.h),
            BaseButton(
                radius: 4,
                title: 'register'.tr,
                onPressed: () {},
                styleButton: BaseButtonStyle.fill)
          ],
        ));
  }
}
