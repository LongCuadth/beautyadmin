import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:get/get.dart';

import '../controllers/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IAuthProvider>(() => AuthProvider(), fenix: true);
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );
  }
}
