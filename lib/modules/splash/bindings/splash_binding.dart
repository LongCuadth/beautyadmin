import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:get/get.dart';

import '../controllers/splash_controller.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IAuthProvider>(() => AuthProvider());
    Get.put<SplashController>(
      SplashController(),
    );
  }
}
