import 'package:app/data/models/category_model.dart';
import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class CategoryManagerController extends GetxController {
  var textEditingController = TextEditingController();
  var scrollController = ScrollController();
  var listCategory = <CategoryModel>[].obs;
  var listSearch = <CategoryModel>[].obs;
  var page = 1;
  final ICategoryProvider _categoryProvider = Get.find<ICategoryProvider>();
  final MessageService _messageService = Get.find<MessageService>();
  var canLoadmore = true.obs;

  @override
  void onInit() {
    getCategories();
    super.onInit();
  }

  void getCategories({bool isLoadMore = false}) async {
    EasyLoading.show();
    var res = await _categoryProvider.getListCategory({
      'size': '10',
      'page': page,
      'category_id': 1,
      'keyword': textEditingController.text,
    });

    if (res.success == true && res.data != null) {
      if (isLoadMore && res.data?.items?.isEmpty == true) {
        canLoadmore.value = false;
      } else {
        listCategory.addAll(res.data?.items ?? []);
        listSearch.assignAll(listCategory);
      }
    }
    EasyLoading.dismiss();
  }

  void loadData() {
    canLoadmore.value = true;
    page = 1;
    listSearch.clear();
    listCategory.clear();
    getCategories();
  }

  void onSearch(String value) {
    Future.delayed(const Duration(milliseconds: 400), () {
      loadData();
    });
  }

  void onRefresh() {
    loadData();
  }

  void onFilterPrice() {
    loadData();
  }

  void loadMore() {
    if (!canLoadmore.value) {
      return;
    }
    page += 1;
    getCategories(isLoadMore: true);
  }

  void deleteItem(CategoryModel item) async {
    EasyLoading.show();
    var response = await _categoryProvider.deleteCategory(item.id ?? 0);
    EasyLoading.dismiss();
    if (response.success == true) {
      listSearch.removeWhere((element) => element.id == item.id);
      listCategory.removeWhere((element) => element.id == item.id);
      listSearch.refresh();
      _messageService
          .send(Message.success(content: 'delete_category_success'.tr));
    } else {
      _messageService
          .send(Message.success(content: 'delete_category_failed'.tr));
    }
  }

  void updateCategory(CategoryModel? data) {
    var index = listSearch.indexWhere((item) => item.id == data?.id);
    listSearch[index] = data ?? CategoryModel();
    listSearch.refresh();
  }
}
