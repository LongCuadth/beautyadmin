import 'package:app/data/models/category_model.dart';
import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/modules/category_manager/controllers/category_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class EditCategoryController extends GetxController {
  final CategoryModel categoryModel;
  final ImagePicker picker = ImagePicker();
  EditCategoryController({required this.categoryModel});
  var categoryController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final _categoryProvider = Get.find<ICategoryProvider>();
  final _messageService = Get.find<MessageService>();

  @override
  void onInit() {
    bindingData();
    super.onInit();
  }

  @override
  void dispose() {
    categoryController.clear();
    super.dispose();
  }

  Future<void> updateCategory() async {
    if (formKey.currentState!.validate()) {
      EasyLoading.show();
      var res = await _categoryProvider.updateCategory(categoryModel.id ?? 0, {
        'name': categoryController.text,
      });
      EasyLoading.dismiss();
      if (res.success == true) {
        Get.back();
        Get.back();
        _messageService
            .send(Message.success(content: 'edit_category_success'.tr));
        Get.find<CategoryManagerController>().updateCategory(res.data);
        categoryController.clear();
      } else {
        _messageService.send(Message.error(content: 'edit_category_failed'.tr));
      }
    }
  }

  void bindingData() {
    categoryController.text = categoryModel.name ?? '';
  }
}
