import 'package:app/data/providers/category/category_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/modules/category_manager/controllers/category_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class AddCategoryController extends GetxController {
  var categoryController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _categoryProvider = Get.find<ICategoryProvider>();
  final _messageService = Get.find<MessageService>();
  final ImagePicker picker = ImagePicker();
  
  @override
  void dispose() {
    categoryController.clear();
    super.dispose();
  }

  Future<void> addCategory() async {
    if (formKey.currentState!.validate()) {
      EasyLoading.show();
      var res = await _categoryProvider.createCategory({
        'name': categoryController.text,
        'category_id': 1,
      });
      EasyLoading.dismiss();
      if (res.success == true) {
        Get.back();
        _messageService
            .send(Message.success(content: 'add_category_success'.tr));
        Get.find<CategoryManagerController>().loadData();
        categoryController.clear();
      } else {
        _messageService.send(Message.error(content: 'add_category_failed'.tr));
      }
    }
  }
}
