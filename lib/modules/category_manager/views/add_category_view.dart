import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/modules/category_manager/controllers/add_category_controller.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class AddCategoryView extends GetWidget<AddCategoryController> {
  const AddCategoryView({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: ThemeProvider.colorWhite),
      child: Form(
          key: controller.formKey,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'add_category'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize16,
                          color: ThemeProvider.colorPrimary),
                    ),
                    GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: const Icon(
                          Icons.close_rounded,
                          size: 24,
                        ))
                  ],
                ),
                SizedBox(height: 30.h),
                BaseTextField(
                    textEditingController: controller.categoryController,
                    validator: (value) {
                      if (value?.isEmpty == true) {
                        return 'invalid_category'.tr;
                      }
                      return null;
                    },
                    hintText: 'category_hint'.tr),
                SizedBox(height: 30.h),
                BaseButton(
                    title: 'confirm'.tr,
                    onPressed: () {
                      controller.addCategory();
                    },
                    styleButton: BaseButtonStyle.fill),
                SizedBox(height: 15.h),
              ],
            ),
          )),
    );
  }
}
