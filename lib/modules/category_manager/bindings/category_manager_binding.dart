import 'package:app/data/providers/category/category_provider.dart';
import 'package:get/get.dart';

import '../controllers/category_manager_controller.dart';

class CategoryManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ICategoryProvider>(() => CategoryProvider(), fenix: true);
    Get.lazyPut<CategoryManagerController>(
      () => CategoryManagerController(),
    );
  }
}
