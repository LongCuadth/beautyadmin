import 'package:app/common/network/shared_preference.dart';
import 'package:app/routes/app_pages.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';

import '../../../data/providers/auth/auth_provider.dart';
import '../../../data/services/message_service.dart';

class MainController extends GetxController {
  var notiUnreadCount = 1.obs;

  var revenueMonth = 10090010.0.obs;
}

class MyDrawerController extends GetxController {
  final zoomDrawerController = ZoomDrawerController();
  final IAuthProvider _authProvider = Get.find<IAuthProvider>();
  final MessageService _messageService = Get.find<MessageService>();
  var name = ''.obs;
  @override
  void onInit() {
    checkName();
    super.onInit();
  }

  void checkName() {
    name.value = SharedPreference.shared.getShop()?.name ?? '';
  }

  void toggleDrawer() {
    zoomDrawerController.toggle?.call();
    update();
  }

  Future<void> logout() async {
    var res = await _authProvider.logOut();
    if (res.success == true) {
      await SharedPreference.shared.remove(SharedPreferenceKey.tokenApiKey);
      await SharedPreference.shared.remove(SharedPreferenceKey.userModelKey);
      Get.offAllNamed(Routes.LOGIN);
    } else {
      _messageService.send(Message.error(content: "log_out_fail".tr));
    }
  }
}
