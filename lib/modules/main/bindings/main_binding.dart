import 'package:get/get.dart';

import '../../../data/providers/auth/auth_provider.dart';
import '../controllers/main_controller.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IAuthProvider>(() => AuthProvider());
    Get.lazyPut<MyDrawerController>(() => MyDrawerController());
    Get.lazyPut<MainController>(
      () => MainController(),
    );
  }
}
