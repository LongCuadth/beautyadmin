import 'package:app/modules/main/controllers/main_controller.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class MenuDrawer extends GetView<MyDrawerController> {
  final MyDrawerController zoomDrawerController;
  const MenuDrawer(this.zoomDrawerController, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        zoomDrawerController.toggleDrawer();
      },
      child: Container(
        padding: const EdgeInsets.all(20),
        color: ThemeProvider.colorBlack.withOpacity(0.05),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Icon(
                Icons.close,
                size: 30,
              ).marginOnly(left: Get.width * 0.65 - 50.w, bottom: 30.h),
              Flexible(
                child: Obx(
                  () => Text(
                    '${'SHOP'.tr} ${zoomDrawerController.name.value}',
                    style: TextStyle(
                        color: ThemeProvider.colorSelect,
                        fontFamily: ThemeProvider.fontBold,
                        fontSize: ThemeProvider.fontSize18),
                  ).marginOnly(bottom: 50.h, right: Get.width * 0.35),
                ),
              ),
              _buildItemMenu(Icons.store, 'shop_info'.tr, () {
                Get.toNamed(Routes.SHOP_INFO)?.then((value) {
                  controller.checkName();
                });
              }),
              // _buildItemMenu(
              //     Icons.shield_outlined, 'change_password'.tr, () {}),
              _buildItemMenu(Icons.logout, 'logout'.tr, () async {
                await controller.logout();
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildItemMenu(IconData icon, String action, VoidCallback onTap) {
    return InkWell(
      onTap: () {
        onTap.call();
      },
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
          Icon(
            icon,
            color: ThemeProvider.colorPrimary,
          ).marginOnly(right: 12.w),
          Text(
            action,
            style: TextStyle(
                color: ThemeProvider.colorBlack,
                fontFamily: ThemeProvider.fontRegular,
                fontSize: ThemeProvider.fontSize18),
          )
        ]).marginOnly(bottom: 20.h),
      ),
    );
  }
}
