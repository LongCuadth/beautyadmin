import 'package:app/gen/assets.gen.dart';
import 'package:app/modules/main/views/menu_drawer.dart';
import 'package:app/routes/app_pages.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/format_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:badges/badges.dart' as badge;
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:get/get.dart';

import '../controllers/main_controller.dart';

class MainView extends GetView<MainController> {
  const MainView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MyDrawerController>(builder: (drawerController) {
      return ZoomDrawer(
        menuScreen: MenuDrawer(drawerController),
        controller: drawerController.zoomDrawerController,
        mainScreen: _mainScreen(drawerController),
        showShadow: false,
        moveMenuScreen: false,
        angle: 0,
        mainScreenScale: 0.2,
        borderRadius: 15,
        menuScreenWidth: MediaQuery.of(context).size.width,
        slideWidth: MediaQuery.of(context).size.width * 0.65,
      );
    });
  }

  Widget _mainScreen(MyDrawerController drawerController) {
    return Scaffold(
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      appBar: AppBar(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            drawerController.toggleDrawer();
          },
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Assets.svg.icMenuBar
                .svg(
                    width: 24.w,
                    colorFilter: const ColorFilter.mode(
                        ThemeProvider.colorBlack, BlendMode.srcIn))
                .marginOnly(left: 16.w),
          ),
        ),
        title: Text(
          'homepage'.tr,
          style: TextStyle(
              color: ThemeProvider.colorBlack,
              fontFamily: ThemeProvider.fontRegular,
              fontSize: ThemeProvider.fontSize24),
        ),
        centerTitle: true,
        actions: [
          Obx(
            () => controller.notiUnreadCount.value > 0
                ? Center(
                    child: badge.Badge(
                      position: badge.BadgePosition.custom(
                          end: 12, bottom: 10, start: 0),
                      badgeContent: Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.all(2),
                      ),
                      badgeStyle: const badge.BadgeStyle(
                        badgeColor: ThemeProvider.colorPrimary,
                        padding: EdgeInsets.all(6),
                      ),
                      child: Assets.svg.icNotification.svg(width: 24.w),
                    ).marginOnly(right: 16.w),
                  )
                : Assets.svg.icNotification.svg().marginOnly(right: 16.w),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 16.w, vertical: 20.h),
        child: Column(
          children: [
            _buildBanner(),
            _buildRevenue(),
            _buildAction(
                action: 'category_manager'.tr,
                onTap: () {
                  Get.toNamed(Routes.CATEGORY_MANAGER);
                }),
            _buildAction(
                action: 'services_manager'.tr,
                onTap: () {
                  Get.toNamed(Routes.SERVICES_MANAGER);
                }),
            _buildAction(
                action: 'employee_manager'.tr,
                onTap: () {
                  Get.toNamed(Routes.EMPLOYEE_MANAGER);
                }),
            _buildAction(
                action: 'booking_manager'.tr,
                onTap: () {
                  Get.toNamed(Routes.BOOKING_MANAGER);
                }),
            _buildAction(
                action: 'customer_manager'.tr,
                onTap: () {
                  Get.toNamed(Routes.CUSTOMER_MANAGER);
                }),
            _buildAction(
                action: 'banner_manager'.tr,
                onTap: () {
                  Get.toNamed(Routes.BANNER_MANAGER);
                }),
          ],
        ),
      ),
    );
  }

  Widget _buildBanner() {
    return Container(
      child: Assets.image.imageBanner.image(),
    );
  }

  Widget _buildRevenue() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20.h),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                '${'revenue_month'.tr} ${DateTime.now().month}',
                style: TextStyle(
                    fontFamily: ThemeProvider.fontMedium,
                    fontSize: ThemeProvider.fontSize16),
              ).marginOnly(right: 5.w),
              const Icon(
                Icons.monetization_on_outlined,
                color: ThemeProvider.colorPrimary,
              )
            ],
          ),
          SizedBox(height: 16.h),
          Text(formatCurrency(controller.revenueMonth.value, '€'),
              style: TextStyle(
                  color: ThemeProvider.colorSelect,
                  fontFamily: ThemeProvider.fontSemiBold,
                  fontSize: ThemeProvider.fontSize16))
        ],
      ),
    );
  }

  Widget _buildAction({required String action, required VoidCallback onTap}) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        onTap.call();
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(action,
              style: TextStyle(
                  color: ThemeProvider.colorBlack,
                  fontFamily: ThemeProvider.fontRegular,
                  fontSize: ThemeProvider.fontSize16)),
          SizedBox(
            height: 10.h,
          ),
          const Divider(
            color: ThemeProvider.colorShadowBox,
            thickness: 0.4,
          )
        ],
      ),
    ).marginSymmetric(vertical: 12.h);
  }
}
