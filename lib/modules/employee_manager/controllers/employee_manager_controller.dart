import 'package:app/data/models/employee_model.dart';
import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class EmployeeManagerController extends GetxController {
  var textEditingController = TextEditingController();
  var listEmployee = <EmployeeModel>[].obs;
  var listSearch = <EmployeeModel>[].obs;
  var scrollController = ScrollController();
  var page = 1;
  var canLoadmore = true.obs;
  final _employeeProvider = Get.find<IEmployeeProvider>();
  final _messageService = Get.find<MessageService>();

  @override
  void onInit() {
    super.onInit();
    getEmployee();
    listSearch.assignAll(listEmployee);
  }

  void getEmployee({bool isLoadMore = false}) async {
    EasyLoading.show();
    var res = await _employeeProvider.getListEmployee({
      'size': '20',
      'page': page,
      'type': 3,
      'keyword': textEditingController.text,
    });

    if (res.success == true && res.data != null) {
      if (isLoadMore && res.data?.items?.isEmpty == true) {
        canLoadmore.value = false;
      } else {
        listEmployee.addAll(res.data?.items ?? []);
        listSearch.assignAll(listEmployee);
      }
    }
    EasyLoading.dismiss();
  }

  void loadData() {
    canLoadmore.value = true;
    page = 1;
    listSearch.clear();
    listEmployee.clear();
    getEmployee();
  }

  void onSearch(String value) {
    Future.delayed(const Duration(milliseconds: 400), () {
      loadData();
    });
  }

  void onRefresh() {
    loadData();
  }

  void onFilterPrice() {
    loadData();
  }

  void loadMore() {
    if (!canLoadmore.value) {
      return;
    }
    page += 1;
    getEmployee(isLoadMore: true);
  }

  void updateEmployee(EmployeeModel? data) {
    var index = listSearch.indexWhere((item) => item.id == data?.id);
    listSearch[index] = data ?? EmployeeModel();
    listEmployee[index] = data ?? EmployeeModel();
    listSearch.refresh();
  }

  Future<void> deleteEmployee(EmployeeModel item) async {
    EasyLoading.show();
    var response = await _employeeProvider.deleteEmployee(item.id ?? 0);

    if (response.success == true) {
      listSearch.removeWhere((element) => element.id == item.id);
      listEmployee.removeWhere((element) => element.id == item.id);
      listSearch.refresh();
      _messageService
          .send(Message.success(content: 'delete_employee_success'.tr));
    } else {
      _messageService
          .send(Message.success(content: 'delete_employee_failed'.tr));
    }
    EasyLoading.dismiss();
  }
}
