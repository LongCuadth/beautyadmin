import 'package:app/data/models/employee_model.dart';
import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/modules/employee_manager/controllers/employee_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class EditEmployeeController extends GetxController {
  final EmployeeModel employeeModel;

  EditEmployeeController({required this.employeeModel});

  var nameController = TextEditingController();

  var phoneController = TextEditingController();

  var emailController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _employeeProvider = Get.find<IEmployeeProvider>();
  final _messageService = Get.find<MessageService>();

  @override
  void onInit() {
    bindingData();
    super.onInit();
  }

  @override
  void dispose() {
    nameController.clear();
    phoneController.clear();
    emailController.clear();
    super.dispose();
  }

  Future<void> updateEmployee() async {
    if (formKey.currentState!.validate()) {
      EasyLoading.show();
      var res = await _employeeProvider.updateEmployee(employeeModel.id ?? 0, {
        'name': nameController.text,
        'phone': phoneController.text,
        'email': emailController.text,
      });
      EasyLoading.dismiss();
      if (res.success == true) {
        Get.back();
        Get.back();
        _messageService
            .send(Message.success(content: 'edit_employee_success'.tr));
        Get.find<EmployeeManagerController>().updateEmployee(res.data);
        nameController.clear();
        phoneController.clear();
        emailController.clear();
      } else {
        _messageService.send(Message.error(content: 'edit_employee_failed'.tr));
      }
    }
  }

  void bindingData() {
    nameController.text = employeeModel.name ?? '';
    phoneController.text = employeeModel.phone ?? '';
    emailController.text = employeeModel.email ?? '';
  }
}
