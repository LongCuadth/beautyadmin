import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/modules/employee_manager/controllers/employee_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class AddEmployeeController extends GetxController {
  var nameController = TextEditingController();

  var phoneController = TextEditingController();

  var emailController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _employeeProvider = Get.find<IEmployeeProvider>();
  final _messageService = Get.find<MessageService>();

  @override
  void dispose() {
    nameController.clear();
    phoneController.clear();
    emailController.clear();
    super.dispose();
  }

  Future<void> addEmployee() async {
    if (formKey.currentState!.validate()) {
      EasyLoading.show();
      var res = await _employeeProvider.createEmployee({
        'name': nameController.text,
        'phone': phoneController.text,
        'email': emailController.text,
        'type': 3
      });
      EasyLoading.dismiss();
      if (res.success == true) {
        Get.back();
        _messageService
            .send(Message.success(content: 'add_employee_success'.tr));
        Get.find<EmployeeManagerController>().loadData();
        nameController.clear();
        phoneController.clear();
        emailController.clear();
      } else {
        _messageService.send(Message.error(content: 'add_employee_failed'.tr));
      }
    }
  }
}
