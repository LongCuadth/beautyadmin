import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/modules/employee_manager/controllers/add_employee_controller.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/validator_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class AddEmployeeView extends GetWidget<AddEmployeeController> {
  const AddEmployeeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: ThemeProvider.colorWhite),
      child: Form(
          key: controller.formKey,
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'add_employee'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize16,
                          color: ThemeProvider.colorPrimary),
                    ),
                    GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: const Icon(
                          Icons.close_rounded,
                          size: 24,
                        ))
                  ],
                ),
                SizedBox(height: 30.h),
                BaseTextField(
                    inputFormatters: [LengthLimitingTextInputFormatter(40)],
                    textEditingController: controller.nameController,
                    validator: (value) {
                      return ValidatorUtils.validatorName(value);
                    },
                    hintText: 'name'.tr),
                SizedBox(height: 20.h),
                BaseTextField(
                    inputType: TextInputType.phone,
                    inputFormatters: [LengthLimitingTextInputFormatter(12)],
                    textEditingController: controller.phoneController,
                    validator: (value) {
                      return ValidatorUtils.validatorPhone(value);
                    },
                    hintText: 'phone'.tr),
                SizedBox(height: 20.h),
                BaseTextField(
                    validator: (value) {
                      return ValidatorUtils.validatorEmail(value, isRequired: true);
                    },
                    inputType: TextInputType.emailAddress,
                    textEditingController: controller.emailController,
                    hintText: 'email'.tr),
                SizedBox(height: 30.h),
                BaseButton(
                    title: 'confirm'.tr,
                    onPressed: () {
                      controller.addEmployee();
                    },
                    styleButton: BaseButtonStyle.fill),
                SizedBox(height: 15.h),
              ],
            ),
          )),
    );
  }
}
