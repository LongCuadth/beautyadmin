import 'package:app/data/providers/employee/employee_provider.dart';
import 'package:get/get.dart';

import '../controllers/employee_manager_controller.dart';

class EmployeeManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IEmployeeProvider>(() => EmployeeProvider());
    Get.lazyPut<EmployeeManagerController>(
      () => EmployeeManagerController(),
    );
  }
}
