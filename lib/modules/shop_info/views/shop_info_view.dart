import 'package:app/base/widgets/base_button.dart';
import 'package:app/base/widgets/base_textfield.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/format_util.dart';
import 'package:app/utils/validator_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/shop_info_controller.dart';

class ShopInfoView extends GetView<ShopInfoController> {
  const ShopInfoView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        appBar: AppBar(
          elevation: 0,
          leading: InkWell(
            radius: 30.r,
            onTap: () {
              Get.back();
            },
            child: const Icon(
              Icons.arrow_back_ios,
              color: ThemeProvider.colorBlack,
              size: 24,
            ),
          ),
          backgroundColor: ThemeProvider.colorBackgroundScreen,
          title: Text('shop_info'.tr,
              style: TextStyle(
                  fontFamily: ThemeProvider.fontLogoBold,
                  fontSize: ThemeProvider.fontSize24,
                  color: ThemeProvider.colorBlack)),
          centerTitle: true,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
                child: Form(
              key: controller.formKey,
              child: SingleChildScrollView(
                padding: const EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 15.h),
                    Text(
                      'name_shop'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    BaseTextField(
                        inputType: TextInputType.name,
                        textEditingController: controller.nameController,
                        validator: (value) {
                          if (value?.isEmpty == true) {
                            return 'empty_name'.tr;
                          }
                          return null;
                        },
                        hintText: 'name_shop'.tr),
                    SizedBox(height: 20.h),
                    Text(
                      'phone'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    BaseTextField(
                        inputType: TextInputType.phone,
                        textEditingController: controller.phoneController,
                        validator: (value) {
                          return ValidatorUtils.validatorPhone(value);
                        },
                        hintText: 'phone'.tr),
                    SizedBox(height: 20.h),
                    Text(
                      'address'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    BaseTextField(
                        inputType: TextInputType.streetAddress,
                        textEditingController: controller.addressController,
                        hintText: 'address'.tr),
                    SizedBox(height: 20.h),
                    Text(
                      'code_shop'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    BaseTextField(
                        inputType: TextInputType.text,
                        textEditingController: controller.codeShopController,
                        hintText: 'code_shop'.tr),
                    SizedBox(height: 20.h),
                    Text(
                      'working_time'.tr,
                      style: TextStyle(
                          fontFamily: ThemeProvider.fontMedium,
                          fontSize: ThemeProvider.fontSize18,
                          color: ThemeProvider.colorPrimary),
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () async {
                            final TimeOfDay? picked = await showTimePicker(
                              initialEntryMode: TimePickerEntryMode.input,
                              context: context,
                              initialTime: TimeOfDay.now(),
                            );
                            if (picked == null) return;
                            controller.startTime.value = picked;
                            controller.startTimeController.text =
                                formatTimeOfDay(picked);
                          },
                          child: IgnorePointer(
                            ignoring: true,
                            child: BaseTextField(
                              inputType: TextInputType.text,
                              textEditingController:
                                  controller.startTimeController,
                              hintText: 'start_time'.tr,
                            ),
                          ),
                        )),
                        SizedBox(
                            width: 40,
                            child: Center(
                              child: Text(
                                '-',
                                style: TextStyle(
                                    fontFamily: ThemeProvider.fontBold,
                                    fontSize: ThemeProvider.fontSize28,
                                    color: ThemeProvider.colorPrimary),
                              ),
                            )),
                        Expanded(
                            child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () async {
                            final TimeOfDay? picked = await showTimePicker(
                              initialEntryMode: TimePickerEntryMode.input,
                              context: context,
                              initialTime: TimeOfDay.now(),
                            );
                            if (picked == null) return;
                            controller.endTime.value = picked;
                            controller.endTimeController.text =
                                formatTimeOfDay(picked);
                          },
                          child: IgnorePointer(
                            ignoring: true,
                            child: BaseTextField(
                                inputType: TextInputType.text,
                                textEditingController:
                                    controller.endTimeController,
                                hintText: 'end_time'.tr),
                          ),
                        )),
                      ],
                    )
                  ],
                ),
              ),
            )),
            BaseButton(
                    title: 'save'.tr,
                    onPressed: () {
                      controller.updateShop();
                    },
                    styleButton: BaseButtonStyle.fill)
                .marginSymmetric(horizontal: 16.w, vertical: 20.h)
          ],
        ));
  }
}
