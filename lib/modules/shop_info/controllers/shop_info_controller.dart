import 'dart:convert';

import 'package:app/common/network/shared_preference.dart';
import 'package:app/data/models/user_model.dart';
import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/utils/format_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ShopInfoController extends GetxController {
  var userModel = UserModel().obs;
  final _authProvider = Get.find<IAuthProvider>();
  var nameController = TextEditingController();
  var phoneController = TextEditingController();
  var addressController = TextEditingController();
  var codeShopController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  final _messageService = Get.find<MessageService>();
  var startTimeController = TextEditingController();
  var endTimeController = TextEditingController();
  var startTime = const TimeOfDay(hour: 9, minute: 0).obs;
  var endTime = const TimeOfDay(hour: 20, minute: 0).obs;
  @override
  void onInit() {
    _getShopModel();
    super.onInit();
  }

  void _getShopModel() async {
    var resData = await _authProvider.getUserInfo();
    if (resData.success == true && resData.data != null) {
      userModel.value = resData.data!;
      nameController.text = userModel.value.shop?.name ?? '';
      phoneController.text = userModel.value.shop?.phone ?? '';
      addressController.text = userModel.value.shop?.address ?? '';
      codeShopController.text = userModel.value.shop?.code ?? '';
      startTimeController.text = userModel.value.shop?.times?.firstOrNull ??
          formatTimeOfDay(startTime.value);
      endTimeController.text = userModel.value.shop?.times?.lastOrNull ??
          formatTimeOfDay(endTime.value);
    }
  }

  Future<void> updateShop() async {
    if (!isValidTimeRange(startTime.value, endTime.value)) {
      _messageService.send(Message.error(content: 'working_time_invalid'.tr));
      return;
    }
    if (formKey.currentState!.validate()) {
      EasyLoading.show();
      var listTime = getTimeRangeStrings(startTime.value, endTime.value);
      var resData =
          await _authProvider.updateShop(userModel.value.shop?.id ?? 0, {
        "name": nameController.text,
        "code": codeShopController.text,
        "phone": phoneController.text,
        "address": addressController.text,
        "times": jsonEncode(listTime)
      });
      if (resData.success == true && resData.data != null) {
        SharedPreference.shared.saveShopModel(resData.data!);
        _messageService.send(Message.success(content: 'edit_shop_success'.tr));
      } else {
        _messageService.send(Message.error(content: resData.message));
      }
      EasyLoading.dismiss();
    }
  }
}
