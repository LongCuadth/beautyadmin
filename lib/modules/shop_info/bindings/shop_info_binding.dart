import 'package:app/data/providers/auth/auth_provider.dart';
import 'package:get/get.dart';

import '../controllers/shop_info_controller.dart';

class ShopInfoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<IAuthProvider>(() => AuthProvider());
    Get.lazyPut<ShopInfoController>(
      () => ShopInfoController(),
    );
  }
}
