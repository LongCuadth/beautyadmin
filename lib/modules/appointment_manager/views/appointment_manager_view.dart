import 'package:app/data/models/booking_model.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/search_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/appointment_manager_controller.dart';

class AppointmentManagerView extends GetView<AppointmentManagerController> {
  const AppointmentManagerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      appBar: AppBar(
        elevation: 0,
        leading: InkWell(
          radius: 30.r,
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back_ios,
            color: ThemeProvider.colorBlack,
            size: 24,
          ),
        ),
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        title: Text('booking'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontLogoBold,
                fontSize: ThemeProvider.fontSize24,
                color: ThemeProvider.colorBlack)),
        centerTitle: true,
      ),
      body: Column(children: [
        SearchWidget(
          showFilterIcon: true,
          hintText: 'search_booking'.tr,
          textEditingController: controller.textEditingController,
          onAddTap: () {},
          onFilterTap: () {},
          onChanged: (value) {
            controller.onSearch(value);
          },
        ),
        SizedBox(height: 20.h),
        Expanded(
            child: NotificationListener(
          onNotification: (notification) {
            if (notification is ScrollEndNotification) {
              if (controller.scrollController.position.extentAfter == 0 &&
                  controller.listSearch.isNotEmpty) {
                controller.loadMore();
              }
            }
            return true;
          },
          child: RefreshIndicator(
            onRefresh: () async {
              controller.onRefresh();
            },
            child: SingleChildScrollView(
                controller: controller.scrollController,
                physics: const AlwaysScrollableScrollPhysics(),
                padding: EdgeInsets.symmetric(vertical: 20.h),
                child: _buildListResult()),
          ),
        ))
      ]),
    );
  }

  Widget _buildListResult() {
    return Obx(() => controller.listSearch.isNotEmpty
        ? ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: controller.listSearch.length,
            itemBuilder: ((context, index) {
              return buildItemBooking(index);
            }))
        : Center(
            child: Text('empty'.tr),
          ).marginOnly(top: Get.height / 3));
  }

  InkWell buildItemBooking(int index) {
    var item = controller.listSearch[index];
    return InkWell(
      onTap: () {
        _openAction(item);
      },
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.w),
          decoration: const BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      color: ThemeProvider.borderButtonOutlineDisable,
                      width: 0.15))),
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'ID: ${item.id}',
                style: TextStyle(
                    fontFamily: ThemeProvider.fontRegular,
                    fontSize: ThemeProvider.fontSize12,
                    color: ThemeProvider.colorSelect),
              ),
              Text(
                'KH: ${item.customer?.name ?? ''}',
                style: TextStyle(
                    fontFamily: ThemeProvider.fontMedium,
                    color: ThemeProvider.colorBlack,
                    fontSize: ThemeProvider.fontSize14),
              ),
              Text(
                'NV: ${item.employee?.name ?? ''}',
                style: TextStyle(
                    fontFamily: ThemeProvider.fontRegular,
                    color: ThemeProvider.colorBlack,
                    fontSize: ThemeProvider.fontSize14),
              ),
              Text('Trạng thái',
                  style: TextStyle(
                      fontFamily: ThemeProvider.fontRegular,
                      color: ThemeProvider.colorBlack,
                      fontSize: ThemeProvider.fontSize14))
            ],
          )),
    );
  }

  void _openAction(BookingModel item) {
    Get.bottomSheet(
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          decoration: const BoxDecoration(
            color: ThemeProvider.colorWhite,
          ),
          height: Get.height * 0.25,
          child: Column(
            children: [
              // _buidAction(
              //     icon: Icons.description_sharp,
              //     value: 'detail_booking'.tr,
              //     onTap: () async {}),
              _buidAction(
                  icon: Icons.edit_document,
                  value: 'edit_booking'.tr,
                  onTap: () async {}),
              _buidAction(
                  icon: Icons.change_circle_outlined,
                  value: 'update_status'.tr,
                  onTap: () async {})
            ],
          ),
        ),
        isScrollControlled: true,
        isDismissible: true);
  }

  _buidAction({IconData? icon, String? value, VoidCallback? onTap}) {
    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.h),
        child: Row(
          children: [
            Icon(
              icon,
              color: ThemeProvider.colorPrimary,
            ),
            SizedBox(width: 40.w),
            Text(
              value ?? '',
              style: TextStyle(
                  fontFamily: ThemeProvider.fontMedium,
                  fontSize: ThemeProvider.fontSize16),
            )
          ],
        ),
      ),
    );
  }
}
