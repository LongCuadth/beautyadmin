import 'package:app/data/models/booking_model.dart';
import 'package:app/data/models/customer_model.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/models/employee_model.dart';

class AppointmentManagerController extends GetxController {
  var textEditingController = TextEditingController();
  var scrollController = ScrollController();
  var listBooking = <BookingModel>[].obs;
  var listSearch = <BookingModel>[].obs;
  var page = 0;

  var listEmployee = [
    EmployeeModel(
        name: 'Olivia Bennett',
        email: 'john@example.com',
        phone: '+49 123456789'),
    EmployeeModel(
        name: 'Olivia Taylor',
        email: 'olivia@example.com',
        phone: '+84 123456789'),
    EmployeeModel(
        name: 'Daniel Wilson',
        email: 'daniel@example.com',
        phone: '+84 123456789'),
  ].obs;

  var listCustomer = [
    CustomerModel(
        name: 'John Doe', email: 'john@example.com', phone: '+49 123456789'),
    CustomerModel(
        name: 'Jane Doe', email: 'jane@example.com', phone: '+49 123456789'),
    CustomerModel(
        name: 'Alice Smith',
        email: 'alice@example.com',
        phone: '+49 123456789'),
  ].obs;

  var listServices = [
    // ServicesModel(
    //     name: 'Naturverstärkung', price: 32, duration: 30, idCategory: '0'),
    // ServicesModel(
    //     name: 'Babyboom, Farbe', price: 89, duration: 120, idCategory: '0'),
    // ServicesModel(
    //     name: 'Farbverlauf ', price: 50, duration: 60, idCategory: '0'),
    // ServicesModel(name: 'Mit Tips', price: 45, duration: 30, idCategory: '0'),
    // ServicesModel(name: 'Ab und Neu', price: 56, duration: 40, idCategory: '0'),
  ].obs;

  @override
  void onInit() {
    // getListCategory();
    // listSearch.assignAll(listBooking);
    super.onInit();
  }

  void getListCategory() {
    genEvents(10);
  }

  void onSearch(String value) {
    listSearch.clear();
  }

  void loadMore() {}

  void onRefresh() {}

  genEvents(int length) {
    listBooking.clear();
    listBooking.addAll(faker.randomGenerator.amount<BookingModel>((i) {
      final hour = faker.randomGenerator.integer(21, min: 12);
      final start = DateTime.now().copyWith(
        hour: hour,
        minute: 30,
        second: 1,
      );

      var bookingModel = BookingModel(
          // id: faker.randomGenerator.integer(length + 1, min: 1).toString(),
          // start: start,
          // end: start.add(
          //   Duration(minutes: faker.randomGenerator.element([60, 90, 120])),
          // ),
          // employeeModel: listEmployee[
          //     faker.randomGenerator.integer(listEmployee.length, min: 0)],
          // customer: listCustomer[
          //     faker.randomGenerator.integer(listCustomer.length, min: 0)],
          // servicesModel: [
          //   listServices[
          //       faker.randomGenerator.integer(listServices.length, min: 0)]
          // ]
          );

      return bookingModel;
    }, length, min: 3));
  }
}
