import 'package:get/get.dart';

import '../controllers/appointment_manager_controller.dart';

class AppointmentManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AppointmentManagerController>(
      () => AppointmentManagerController(),
    );
  }
}
