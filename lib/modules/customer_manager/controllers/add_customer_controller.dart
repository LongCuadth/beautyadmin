import 'package:app/data/providers/customer/customer_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/modules/customer_manager/controllers/customer_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class AddCustomerController extends GetxController {
  var nameController = TextEditingController();

  var phoneController = TextEditingController();

  var emailController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _customerProvider = Get.find<ICustomerProvider>();
  final _messageService = Get.find<MessageService>();

  @override
  void onClose() {
    nameController.clear();
    phoneController.clear();
    emailController.clear();
    super.onClose();
  }

  Future<void> addCustomer() async {
    if (formKey.currentState!.validate()) {
      EasyLoading.show();
      var res = await _customerProvider.createCustomer({
        'name': nameController.text,
        'phone': phoneController.text,
        'email': emailController.text,
        'type': 4
      });
      EasyLoading.dismiss();
      if (res.success == true) {
        Get.back();
        _messageService
            .send(Message.success(content: 'add_customer_success'.tr));
        Get.find<CustomerManagerController>().loadData();
        nameController.clear();
        phoneController.clear();
        emailController.clear();
      } else {
        _messageService.send(Message.error(content: 'add_customer_failed'.tr));
      }
    }
  }
}
