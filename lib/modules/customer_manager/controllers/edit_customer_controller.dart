import 'package:app/data/models/customer_model.dart';
import 'package:app/data/providers/customer/customer_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:app/modules/customer_manager/controllers/customer_manager_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class EditCustomerController extends GetxController {
  final CustomerModel customerModel;

  EditCustomerController({required this.customerModel});

  var nameController = TextEditingController();

  var phoneController = TextEditingController();

  var emailController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _customerProvider = Get.find<ICustomerProvider>();
  final _messageService = Get.find<MessageService>();

  @override
  void onInit() {
    bindingData();
    super.onInit();
  }

  @override
  void onClose() {
    nameController.clear();
    phoneController.clear();
    emailController.clear();
    super.onClose();
  }

  Future<void> updateCustomer() async {
    if (formKey.currentState!.validate()) {
      EasyLoading.show();
      var res = await _customerProvider.updateCustomer(customerModel.id ?? 0, {
        'name': nameController.text,
        'phone': phoneController.text,
        'email': emailController.text,
      });
      EasyLoading.dismiss();
      if (res.success == true) {
        Get.back();
        Get.back();
        _messageService
            .send(Message.success(content: 'edit_customer_success'.tr));
        Get.find<CustomerManagerController>().updateCustomer(res.data);
        nameController.clear();
        phoneController.clear();
        emailController.clear();
      } else {
        _messageService.send(Message.error(content: 'edit_customer_failed'.tr));
      }
    }
  }

  void bindingData() {
    nameController.text = customerModel.name ?? '';
    phoneController.text = customerModel.phone ?? '';
    emailController.text = customerModel.email ?? '';
  }
}
