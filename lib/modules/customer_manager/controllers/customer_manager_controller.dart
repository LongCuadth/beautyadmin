import 'package:app/data/models/customer_model.dart';
import 'package:app/data/providers/customer/customer_provider.dart';
import 'package:app/data/services/message_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class CustomerManagerController extends GetxController {
  var textEditingController = TextEditingController();
  var listCustomer = <CustomerModel>[].obs;
  var listSearch = <CustomerModel>[].obs;
  var scrollController = ScrollController();
  var page = 1;
  var canLoadmore = true.obs;
  final _customerProvider = Get.find<ICustomerProvider>();
  final _messageService = Get.find<MessageService>();

  @override
  void onInit() {
    super.onInit();
    getCustomer();
    listSearch.assignAll(listCustomer);
  }

  void getCustomer({bool isLoadMore = false}) async {
    EasyLoading.show();
    var res = await _customerProvider.getListCustomer({
      'size': '20',
      'page': page,
      'type': 4,
      'keyword': textEditingController.text,
    });

    if (res.success == true && res.data != null) {
      if (isLoadMore && res.data?.items?.isEmpty == true) {
        canLoadmore.value = false;
      } else {
        listCustomer.addAll(res.data?.items ?? []);
        listSearch.assignAll(listCustomer);
      }
    }
    EasyLoading.dismiss();
  }

  void loadData() {
    canLoadmore.value = true;
    page = 1;
    listSearch.clear();
    listCustomer.clear();
    getCustomer();
  }

  void onSearch(String value) {
    Future.delayed(const Duration(milliseconds: 400), () {
      loadData();
    });
  }

  void onRefresh() {
    loadData();
  }

  void onFilterPrice() {
    loadData();
  }

  void loadMore() {
    if (!canLoadmore.value) {
      return;
    }
    page += 1;
    getCustomer(isLoadMore: true);
  }

  void updateCustomer(CustomerModel? data) {
    var index = listSearch.indexWhere((item) => item.id == data?.id);
    listSearch[index] = data ?? CustomerModel();
    listCustomer[index] = data ?? CustomerModel();
    listSearch.refresh();
  }

  Future<void> deleteCustomer(CustomerModel item) async {
    EasyLoading.show();
    var response = await _customerProvider.deleteCustomer(item.id ?? 0);

    if (response.success == true) {
      listSearch.removeWhere((element) => element.id == item.id);
      listCustomer.removeWhere((element) => element.id == item.id);
      listSearch.refresh();
      _messageService
          .send(Message.success(content: 'delete_customer_success'.tr));
    } else {
      _messageService
          .send(Message.success(content: 'delete_customer_failed'.tr));
    }
    EasyLoading.dismiss();
  }
}
