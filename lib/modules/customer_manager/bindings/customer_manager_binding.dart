import 'package:app/data/providers/customer/customer_provider.dart';
import 'package:get/get.dart';

import '../controllers/customer_manager_controller.dart';

class CustomerManagerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ICustomerProvider>(() => CustomerProvider());
    Get.lazyPut<CustomerManagerController>(
      () => CustomerManagerController(),
    );
  }
}
