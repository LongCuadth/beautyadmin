import 'package:app/data/models/customer_model.dart';
import 'package:app/modules/customer_manager/controllers/add_customer_controller.dart';
import 'package:app/modules/customer_manager/controllers/edit_customer_controller.dart';
import 'package:app/modules/customer_manager/views/add_customer_view.dart';
import 'package:app/modules/customer_manager/views/edit_customer_view.dart';
import 'package:app/themes/themes_provider.dart';
import 'package:app/utils/search_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../controllers/customer_manager_controller.dart';

class CustomerManagerView extends GetView<CustomerManagerController> {
  const CustomerManagerView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeProvider.colorBackgroundScreen,
      appBar: AppBar(
        elevation: 0,
        leading: InkWell(
          radius: 30.r,
          onTap: () {
            Get.back();
          },
          child: const Icon(
            Icons.arrow_back_ios,
            color: ThemeProvider.colorBlack,
            size: 24,
          ),
        ),
        backgroundColor: ThemeProvider.colorBackgroundScreen,
        title: Text('customer'.tr,
            style: TextStyle(
                fontFamily: ThemeProvider.fontLogoBold,
                fontSize: ThemeProvider.fontSize24,
                color: ThemeProvider.colorBlack)),
        centerTitle: true,
      ),
      body: Column(children: [
        SearchWidget(
          showFilterIcon: false,
          hintText: 'search_customer'.tr,
          textEditingController: controller.textEditingController,
          onAddTap: () {
            Get.put<AddCustomerController>(AddCustomerController());
            Get.bottomSheet(const AddCustomerView(), isDismissible: true);
          },
          onFilterTap: () {},
          onChanged: (value) {
            controller.onSearch(value);
          },
        ),
        SizedBox(height: 20.h),
        Expanded(
          child: NotificationListener(
            onNotification: (notification) {
              if (notification is ScrollEndNotification) {
                if (controller.scrollController.position.extentAfter == 0 &&
                    controller.listSearch.isNotEmpty) {
                  controller.loadMore();
                }
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: () async {
                controller.onRefresh();
              },
              child: SingleChildScrollView(
                  controller: controller.scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(vertical: 20.h),
                  child: _buildListResult()),
            ),
          ),
        ),
      ]),
    );
  }

  Widget _buildListResult() {
    return Obx(() => controller.listSearch.isNotEmpty
        ? ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: controller.listSearch.length,
            itemBuilder: ((context, index) {
              return InkWell(
                onTap: () {
                  _openAction(controller.listSearch[index]);
                },
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.w),
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: ThemeProvider.borderButtonOutlineDisable,
                              width: 0.15))),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        controller.listSearch[index].name ?? '',
                        style: TextStyle(
                            fontFamily: ThemeProvider.fontMedium,
                            fontSize: ThemeProvider.fontSize16),
                      ),
                      SizedBox(height: 5.h),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            controller.listSearch[index].phone ?? '',
                            style: TextStyle(
                                fontFamily: ThemeProvider.fontRegular,
                                fontSize: ThemeProvider.fontSize12,
                                color: ThemeProvider.colorSelect),
                          ),
                          Text(
                            controller.listSearch[index].email ?? '',
                            style: TextStyle(
                                fontFamily: ThemeProvider.fontRegular,
                                fontSize: ThemeProvider.fontSize12,
                                color: ThemeProvider.colorBlack),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }))
        : Center(
            child: Text('empty'.tr),
          ).marginOnly(top: Get.height / 3));
  }

  void _openAction(CustomerModel item) {
    Get.bottomSheet(
        Container(
          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
          decoration: const BoxDecoration(
            color: ThemeProvider.colorWhite,
          ),
          height: Get.height * 0.3,
          child: Column(
            children: [
              _buidAction(
                  icon: Icons.edit_document,
                  value: 'edit_info'.tr,
                  onTap: () {
                    Get.put(EditCustomerController(customerModel: item));
                    Get.bottomSheet(const EditCustomerView()).then((value) {
                      controller.listSearch.refresh();
                    });
                  }),
              _buidAction(
                  icon: Icons.call_outlined,
                  value: '${'call'.tr} ${item.phone ?? ''}',
                  onTap: () async {
                    final Uri phoneLaunchUri =
                        Uri(scheme: 'tel', path: item.phone);
                    if (await canLaunchUrl(phoneLaunchUri)) {
                      await launchUrl(phoneLaunchUri);
                    } else {
                      debugPrint('Could not launch $phoneLaunchUri');
                    }
                  }),
              _buidAction(
                  icon: Icons.delete_forever_outlined,
                  value: 'delete_customer'.tr,
                  onTap: () {
                    controller.deleteCustomer(item);
                    Get.back();
                  })
            ],
          ),
        ),
        isScrollControlled: true,
        isDismissible: true);
  }

  _buidAction({IconData? icon, String? value, VoidCallback? onTap}) {
    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20.h, horizontal: 20.h),
        child: Row(
          children: [
            Icon(
              icon,
              color: ThemeProvider.colorPrimary,
            ),
            SizedBox(width: 40.w),
            Text(
              value ?? '',
              style: TextStyle(
                  fontFamily: ThemeProvider.fontMedium,
                  fontSize: ThemeProvider.fontSize16),
            )
          ],
        ),
      ),
    );
  }
}
