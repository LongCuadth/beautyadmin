import 'package:app/flavors.dart';

class NetworkConfig {
  static String get baseAPI => BuildConfig.baseDomain;
}

class ApiPath {
  // AUTH
  static String apiLogin = '/api/login';
  static String checkphone = '/api/auth/check-phone';
  static String apiGetUserInfo = "/api/me";
  static String apiLogout = "/api/logout";
  static String apiRefreshToken = '/api/refresh';
  static String apiUpdateShop = '/api/shop/';

  // Category
  static String deleteCategory = '/api/service/';
  static String updateCategory = '/api/service/';

  // Service
  static String deleteService = '/api/product/';
  static var updateService = '/api/product/';

  // Employee
  static var deleteEmployee = '/api/user/';
  static var updateEmployee = '/api/user/';

  // Customer
  static var deleteCustomer = '/api/user/';
  static var updateCustomer = '/api/user/';

  // Booking
  static var deleteBooking = '/api/order/';
  static var updateBooking = '/api/order/';
  static var getListBooking = '/api/order/';
  // Banner
  static var deleteBanner = '/api/banner/';
}
