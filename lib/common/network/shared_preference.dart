import 'package:app/data/models/shop_model.dart';
import 'package:app/data/models/user_model.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class SharedPreferenceKey {
  static const String tokenApiKey = 'tokenApi';
  static const String userModelKey = 'userModel';
  static const String shopModelKey = 'shopModel';
}

class SharedPreference {
  static final shared = SharedPreference();

  Box? _hiveBox;

  Future<void> openHiveBox() async {
    var dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    _hiveBox = await Hive.openBox("BeautyAdmin");
  }

  read(String key) {
    return _hiveBox?.get(key);
  }

  Future<void> save(String key, value) async {
    await _hiveBox?.put(key, value);
  }

  Future<void> remove(String key) async {
    await _hiveBox?.delete(key);
  }

  Future<void> saveUser(UserModel user) async {
    final data = user.toJson();
    await save(SharedPreferenceKey.userModelKey, data);
  }

  Future<void> saveShopModel(ShopModel shopModel) async {
    final data = shopModel.toJson();
    await save(SharedPreferenceKey.shopModelKey, data);
  }

  ShopModel? getShop() {
    final data = _hiveBox?.get(SharedPreferenceKey.shopModelKey);
    if (data != null) {
      final map = Map<String, dynamic>.from(data);
      final user = ShopModel.fromJson(map);
      return user;
    }
    return null;
  }

  UserModel? getUser() {
    final data = _hiveBox?.get(SharedPreferenceKey.userModelKey);
    if (data != null) {
      final map = Map<String, dynamic>.from(data);
      final user = UserModel.fromJson(map);
      return user;
    }
    return null;
  }

  Future<void> saveToken(String token) async {
    await save(SharedPreferenceKey.tokenApiKey, token);
  }

  Future<String> getToken() async {
    final data = _hiveBox?.get(SharedPreferenceKey.tokenApiKey);
    if (data != null && data is String) {
      return data;
    }
    return '';
  }
}
